#### Table of contents

[[_TOC_]]

# Setting up ASM service mesh across multiple service projects on a shared VPC using private clusters

This tutorial shows how to run distributed services on multiple GKE clusters in multiple service projects on a shared VPC in GCP using Anthos Service Mesh (ASM). A distributed service is a Kubernetes Service that runs on multiple Kubernetes clusters in the same namespace and acts as a single logical service. Distributed services are more resilient than Kubernetes Services because they run on multiple GKE clusters. A distributed service remains up even if one or more GKE clusters are down, as long as the healthy clusters are able to serve the desired load.   
Kubernetes Services are known only to the Kubernetes API server of the cluster they run on. If the Kubernetes cluster is down (for example during a scheduled maintenance), so are all of the Kubernetes Services running on that cluster. Running distributed services makes cluster lifecycle management easier since clusters can be brought down for maintenance or upgrades while other cluster service service traffic. In order to create a distributed service, service mesh functionality provided by ASM is used to "glue" services running on multiple clusters together to act as a single logical service.  
This guide shows how to set up a shared VPC with multiple service projects. Then, how to create multiple GKE private clusters in multiple service projects, how to deploy ASM on multiple GKE private clusters and how to run distributed services on this platform. You may use the same guide for non-private GKE clusters as well. The configuration intended strictly for private clusters is highlighted in this guide.  
This guide is intended for platform administrators and service operators with basic knowledge of Kubernetes. Some knowledge of service mesh is beneficial although not necessary. ASM is based on the OSS Istio technology. You can learn more about service mesh and Istio on [istio.io](istio.io).  

# Organization and Architecture

For the purpose of this guide, imagine your organization is divided into different teams based on functions. There are four teams in your organization:

+   **Networking team** - a centralized team of cloud networking experts responsible for managing GCP networks. They own, manage and maintain the VPC and other networking resources for the entire organization.
+   **Product bank team** - an application team responsible for creating the `bank` application or product. This team manages and maintains all resources to run the `bank` application. They maintain and manage their own Kubernetes clusters inside their own GCP project. 
+   **Product shop team** - an application team responsible for creating the `shop` application or product. This team manages and maintains all resources to run the `shop` application. They maintain and manage their own Kubernetes clusters inside their own GCP project.
+   **Platform team** - in some cases, you may have a centralized platform administrator team. This team is responsible for maintaining and managing the platform where application teams run their applications. This team is responsible for GKE and service mesh administration. 

In this guide, you are responsible for building a cloud architecture to run the two products - `bank` and `shop` in their own GCP projects. The high level architecture looks as follows:

&nbsp;
<img src="/docs/arch.png" width=100% height=100%>
&nbsp;

## Objective

+   Create three projects. One for the shared VPC (also the network host project) and two for the `bank` and `shop` product.
+   Create and configure a shared VPC, create subnets and share subnets with the two service projects.
+   Create two GKE private clusters in each service project (`bank` and `shop` project).
+   Configure networking (NAT Gateways, Cloud Router and firewall rules) to allow inter-cluster and egress traffic from the four private GKE clusters. Also allow API service access from your terminal (Cloud Shell) to the four private GKE clusters using authorized networks.
+   Deploy and configure multi project and multi cluster ASM to the four GKE clusters in multi-primary mode. Multi-primary mode deploys ASM controlplane in all clusters.
+   Deploy the Bank of Anthos application on the two clusters in the `bank` project. All services except the databases are deployed as distributed services (Pods running on both clusters).
+   Deploy the Online Boutique application on the two clusters in the `shop` project. All services except the Redis database are deployed as distributed services (Pods running on both clusters).
+   Access the two applications via ASM Ingress.

## Costs

This tutorial uses the following billable components of Google Cloud Platform:

+   [GKE](https://cloud.google.com/kubernetes-engine/pricing)
+   [Networking](https://cloud.google.com/vpc/network-pricing#vpc-pricing)
+   [Load Balancing](https://cloud.google.com/vpc/network-pricing#lb)

Use the [Pricing Calculator](https://cloud.google.com/products/calculator) to generate a cost estimate based on your projected usage.  

## Before you begin

1.  In the Cloud Console, activate Cloud Shell.

    [ACTIVATE CLOUD SHELL](https://console.cloud.google.com/?cloudshell=true)

At the bottom of the [Cloud Console](https://cloud.google.com/shell/docs/features), a Cloud Shell session starts and displays a command-line prompt. Cloud Shell is a shell environment with the Cloud SDK already installed, including the [gcloud](https://cloud.google.com/sdk/gcloud) command-line tool, and with values already set for your current project. It can take a few seconds for the session to initialize.  
This guide requires a GCP organization because of shared VPC. Your user must also be an Organization Admin and a Billing Account Admin so you can create resources in the org and assign billing accounts to the new projects.  

## Setting up the environment

1.  Create environment variables used in this guide. In case you lose access to your terminal (and your envars), you can rerun this section.

    ```bash
    export ADMIN_USER=<Enter Org admin email>
    export ORG_ID=<Enter Org ID>
    export BILLING_ACCOUNT=<Enter Billing Account ID>
    export FOLDER_NAME=asm18-shared-vpc
    export HOST_NET_PROJECT=proj-0-net-prod
    export SVC_1_PROJECT=proj-1-bank-prod
    export SVC_2_PROJECT=proj-2-shop-prod

    # Shared VPC
    export SHARED_VPC=shared-vpc
    export SUBNET_1_REGION=us-west2
    export SUBNET_1_NAME=subnet-1-${SUBNET_1_REGION}
    export SUBNET_1_RANGE=10.4.0.0/22
    export SUBNET_1_POD_NAME=${SUBNET_1_NAME}-pods
    export SUBNET_1_POD_RANGE=10.0.0.0/14
    export SUBNET_1_SVC_1_NAME=${SUBNET_1_NAME}-svc-1
    export SUBNET_1_SVC_1_RANGE=10.5.0.0/20
    export SUBNET_1_SVC_2_NAME=${SUBNET_1_NAME}-svc-2
    export SUBNET_1_SVC_2_RANGE=10.5.16.0/20
    export SUBNET_1_SVC_3_NAME=${SUBNET_1_NAME}-svc-3
    export SUBNET_1_SVC_3_RANGE=10.5.32.0/20
    export SUBNET_2_REGION=us-central1
    export SUBNET_2_NAME=subnet-2-${SUBNET_1_REGION}
    export SUBNET_2_RANGE=10.12.0.0/22
    export SUBNET_2_POD_NAME=${SUBNET_2_NAME}-pods
    export SUBNET_2_POD_RANGE=10.8.0.0/14
    export SUBNET_2_SVC_1_NAME=${SUBNET_2_NAME}-svc-1
    export SUBNET_2_SVC_1_RANGE=10.13.0.0/20
    export SUBNET_2_SVC_2_NAME=${SUBNET_2_NAME}-svc-2
    export SUBNET_2_SVC_2_RANGE=10.13.16.0/20
    export SUBNET_2_SVC_3_NAME=${SUBNET_2_NAME}-svc-3
    export SUBNET_2_SVC_3_RANGE=10.13.32.0/20
    export ASM_MAJOR_VERSION=1.8
    export ASM_VERSION=1.8.1-asm.5
    export ASM_LABEL=asm-181-5
    export CLOUDSHELL_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)

    # GKE clusters
    export GKE1=gke-1-r1a-prod
    export GKE2=gke-2-r1b-prod
    export GKE3=gke-3-r2a-prod
    export GKE4=gke-4-r2b-prod
    export GKE1_ZONE=${SUBNET_1_REGION}-a
    export GKE2_ZONE=${SUBNET_1_REGION}-b
    export GKE3_ZONE=${SUBNET_2_REGION}-a
    export GKE4_ZONE=${SUBNET_2_REGION}-b
    export GKE1_MASTER_IPV4_CIDR=172.16.0.0/28
    export GKE2_MASTER_IPV4_CIDR=172.16.1.0/28
    export GKE3_MASTER_IPV4_CIDR=172.16.2.0/28
    export GKE4_MASTER_IPV4_CIDR=172.16.3.0/28
    export GKE_INGRESS=ingress-config # for future use
    export GKE_INGRESS_ZONE=us-west2-a # for future use
    ```

1.  Install [krew](https://krew.sigs.k8s.io). Krew is a kubectl plugin manager that helps you discover and manage kubectl plugins. Two of these plugins are ctx and ns which make switching between multiple GKE clusters easier.

    ```bash
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )

    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc
    ```

1.  Install `ctx` and `ns` via krew for easy context switching.

    ```bash
    kubectl krew install ctx
    kubectl krew install ns
    ```

## Creating folder and projects

In this section, you create three GCP projects and set up billing on the projects.

1.  Create a folder for the three projects. Creating a folder allows you to easily keep track of resources required for this guide in one place. At the end, you can delete all resources in this folder.

    ```bash
    gcloud config set account ${ADMIN_USER}

    gcloud alpha resource-manager folders create \
    --display-name=${FOLDER_NAME} \
    --organization=${ORG_ID}
    ```

1.  Get folder ID.

    ```bash
    export FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} | grep ${FOLDER_NAME} | awk '{print $3}')
    ```

1.  Create three projects inside the folder and enable billing on all three projects.

    ```bash
    gcloud projects create ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --folder ${FOLDER_ID}
    export HOST_NET_PROJECT_NUM=$(gcloud projects describe ${HOST_NET_PROJECT}-${FOLDER_ID} --format='value(projectNumber)')
    gcloud beta billing projects link ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}

    gcloud projects create ${SVC_1_PROJECT}-${FOLDER_ID} \
    --folder ${FOLDER_ID}
    export SVC_1_PROJECT_NUM=$(gcloud projects describe ${SVC_1_PROJECT}-${FOLDER_ID} --format='value(projectNumber)')
    gcloud beta billing projects link ${SVC_1_PROJECT}-${FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}

    gcloud projects create ${SVC_2_PROJECT}-${FOLDER_ID} \
    --folder ${FOLDER_ID}
    export SVC_2_PROJECT_NUM=$(gcloud projects describe ${SVC_2_PROJECT}-${FOLDER_ID} --format='value(projectNumber)')
    gcloud beta billing projects link ${SVC_2_PROJECT}-${FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}
    ```

## Setting up shared VPC

In this section, you create a shared VPC in the network host project with 2 subnets. You share 1 subnet with each of the two service projects.

1.  Enable the required APIs on all three projects.

    ```bash
    gcloud services enable \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    container.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    anthos.googleapis.com \
    multiclusteringress.googleapis.com \
    cloudresourcemanager.googleapis.com

    gcloud services enable \
    --project=${SVC_1_PROJECT}-${FOLDER_ID} \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    cloudresourcemanager.googleapis.com \
    anthos.googleapis.com \
    multiclusteringress.googleapis.com \
    stackdriver.googleapis.com

    gcloud services enable \
    --project=${SVC_2_PROJECT}-${FOLDER_ID} \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    cloudresourcemanager.googleapis.com \
    anthos.googleapis.com \
    multiclusteringress.googleapis.com \
    stackdriver.googleapis.com
    ```

1.  Create a shared VPC and 2 subnets with secondary IP address ranges.

    ```bash
    gcloud compute networks create ${SHARED_VPC} \
    --subnet-mode custom \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID}

    gcloud compute networks subnets create ${SUBNET_1_NAME} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --network ${SHARED_VPC} \
    --range ${SUBNET_1_RANGE} \
    --region ${SUBNET_1_REGION} \
    --secondary-range ${SUBNET_1_POD_NAME}=${SUBNET_1_POD_RANGE},${SUBNET_1_SVC_1_NAME}=${SUBNET_1_SVC_1_RANGE},${SUBNET_1_SVC_2_NAME}=${SUBNET_1_SVC_2_RANGE},${SUBNET_1_SVC_3_NAME}=${SUBNET_1_SVC_3_RANGE}

    gcloud compute networks subnets create ${SUBNET_2_NAME} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --network ${SHARED_VPC} \
    --range ${SUBNET_2_RANGE} \
    --region ${SUBNET_2_REGION} \
    --secondary-range ${SUBNET_2_POD_NAME}=${SUBNET_2_POD_RANGE},${SUBNET_2_SVC_1_NAME}=${SUBNET_2_SVC_1_RANGE},${SUBNET_2_SVC_2_NAME}=${SUBNET_2_SVC_2_RANGE},${SUBNET_2_SVC_3_NAME}=${SUBNET_2_SVC_3_RANGE}
    ```

Enable shared VPC and share two subnets with the two service projects.

1.  Get the GKE and Google API service accounts from both service projects.

    ```bash
    export SVC_1_PROJECT_GKE_SA=service-${SVC_1_PROJECT_NUM}@container-engine-robot.iam.gserviceaccount.com
    export SVC_1_PROJECT_GOOGLEAPIS_SA=${SVC_1_PROJECT_NUM}@cloudservices.gserviceaccount.com
    export SVC_2_PROJECT_GKE_SA=service-${SVC_2_PROJECT_NUM}@container-engine-robot.iam.gserviceaccount.com
    export SVC_2_PROJECT_GOOGLEAPIS_SA=${SVC_2_PROJECT_NUM}@cloudservices.gserviceaccount.com
    ```

1.  Enable shared VPC on the host project.

    ```bash
    gcloud compute shared-vpc enable ${HOST_NET_PROJECT}-${FOLDER_ID}
    ```

1.  Associate the two service projects with the shared VPC.

    ```bash
    gcloud compute shared-vpc associated-projects add ${SVC_1_PROJECT}-${FOLDER_ID} \
    --host-project ${HOST_NET_PROJECT}-${FOLDER_ID}

    gcloud compute shared-vpc associated-projects add ${SVC_2_PROJECT}-${FOLDER_ID} \
    --host-project ${HOST_NET_PROJECT}-${FOLDER_ID}
    ```

1.  Update the IAM policies to allow the service projects to be network users of the shared VPC. This allows the service projects to consume network resources from the host project.

    ```bash
    export SUBNET_1_ETAG=$(gcloud compute networks subnets get-iam-policy ${SUBNET_1_NAME} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --region ${SUBNET_1_REGION} --format=json | jq -r '.etag')

    cat <<EOF > subnet-1-policy.yaml
    bindings:
    - members:
      - serviceAccount:${SVC_1_PROJECT_GKE_SA}
      - serviceAccount:${SVC_1_PROJECT_GOOGLEAPIS_SA}
      role: roles/compute.networkUser
    etag: ${SUBNET_1_ETAG}
    EOF

    gcloud compute networks subnets set-iam-policy ${SUBNET_1_NAME} \
    subnet-1-policy.yaml \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --region ${SUBNET_1_REGION}

    export SUBNET_2_ETAG=$(gcloud compute networks subnets get-iam-policy ${SUBNET_2_NAME} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --region ${SUBNET_2_REGION} --format=json | jq -r '.etag')

    cat <<EOF > subnet-2-policy.yaml
    bindings:
    - members:
      - serviceAccount:${SVC_2_PROJECT_GKE_SA}
      - serviceAccount:${SVC_2_PROJECT_GOOGLEAPIS_SA}
      role: roles/compute.networkUser
    etag: ${SUBNET_2_ETAG}
    EOF

    gcloud compute networks subnets set-iam-policy ${SUBNET_2_NAME} \
    subnet-2-policy.yaml \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --region ${SUBNET_2_REGION}
    ```

1.  If you want the GKE cluster to create and manage the firewall resources in your host project, grant the service projects' GKE SAs, Compute Security Admin role in the host network project.

    ```bash
    gcloud projects add-iam-policy-binding ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --member=serviceAccount:${SVC_1_PROJECT_GKE_SA} \
    --role=roles/compute.securityAdmin

    gcloud projects add-iam-policy-binding ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --member=serviceAccount:${SVC_2_PROJECT_GKE_SA} \
    --role=roles/compute.securityAdmin
    ```

1.  Each service project's GKE service account must have a binding for the Host Service Agent User role on the host project.

    ```bash
    gcloud projects add-iam-policy-binding ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --member serviceAccount:${SVC_1_PROJECT_GKE_SA} \
    --role roles/container.hostServiceAgentUser

    gcloud projects add-iam-policy-binding ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --member serviceAccount:${SVC_2_PROJECT_GKE_SA} \
    --role roles/container.hostServiceAgentUser
    ```

1.  Verify shared VPC config for service project 1.

    ```bash
    gcloud container subnets list-usable \
    --project ${SVC_1_PROJECT}-${FOLDER_ID} \
    --network-project ${HOST_NET_PROJECT}-${FOLDER_ID}
    ```

The output is similar to the following:  

```
    PROJECT                       REGION    NETWORK     SUBNET             RANGE
    proj-0-net-prod-814581352178  us-west2  shared-vpc  subnet-1-us-west2  10.4.0.0/22
        ┌─────────────────────────┬───────────────┬─────────────────────────────┐
        │   SECONDARY_RANGE_NAME  │ IP_CIDR_RANGE │            STATUS           │
        ├─────────────────────────┼───────────────┼─────────────────────────────┤
        │ subnet-1-us-west2-pods  │ 10.0.0.0/14   │ usable for pods or services │
        │ subnet-1-us-west2-svc-1 │ 10.5.0.0/20   │ usable for pods or services │
        │ subnet-1-us-west2-svc-2 │ 10.5.16.0/20  │ usable for pods or services │
        │ subnet-1-us-west2-svc-3 │ 10.5.32.0/20  │ usable for pods or services │
        └─────────────────────────┴───────────────┴─────────────────────────────┘
```

1. Verify shared VPC config for service project 2.

    ```bash
    gcloud container subnets list-usable \
    --project ${SVC_2_PROJECT}-${FOLDER_ID} \
    --network-project ${HOST_NET_PROJECT}-${FOLDER_ID}
    ```
 

The output is similar to the following:  

```
    PROJECT                       REGION       NETWORK     SUBNET             RANGE
    proj-0-net-prod-814581352178  us-central1  shared-vpc  subnet-2-us-west2  10.12.0.0/22
        ┌─────────────────────────┬───────────────┬─────────────────────────────┐
        │   SECONDARY_RANGE_NAME  │ IP_CIDR_RANGE │            STATUS           │
        ├─────────────────────────┼───────────────┼─────────────────────────────┤
        │ subnet-2-us-west2-pods  │ 10.8.0.0/14   │ usable for pods or services │
        │ subnet-2-us-west2-svc-1 │ 10.13.0.0/20  │ usable for pods or services │
        │ subnet-2-us-west2-svc-2 │ 10.13.16.0/20 │ usable for pods or services │
        │ subnet-2-us-west2-svc-3 │ 10.13.32.0/20 │ usable for pods or services │
        └─────────────────────────┴───────────────┴─────────────────────────────┘
```

## Configure Firewall rules and Cloud NAT for private clusters

In this section, you create the required firewall rules and Cloud Nat gateways for GKE private clusters. Firewall rules allow for pod to pod connectivity in the mesh and Cloud Nat gateways are required for external facing (internet facing) traffic.

1.  Create external IPs to be consumed later in the process:

    ```bash
    gcloud compute addresses create ${SUBNET_1_REGION}-nat-ip --project=${HOST_NET_PROJECT}-${FOLDER_ID} --region=${SUBNET_1_REGION}
    gcloud compute addresses create ${SUBNET_2_REGION}-nat-ip --project=${HOST_NET_PROJECT}-${FOLDER_ID} --region=${SUBNET_2_REGION}

    export NAT_REGION_1_IP_ADDR=$(gcloud compute addresses describe ${SUBNET_1_REGION}-nat-ip --project=${HOST_NET_PROJECT}-${FOLDER_ID} --region=${SUBNET_1_REGION} --format='value(address)')
    export NAT_REGION_1_IP_NAME=$(gcloud compute addresses describe ${SUBNET_1_REGION}-nat-ip --project=${HOST_NET_PROJECT}-${FOLDER_ID} --region=${SUBNET_1_REGION} --format='value(name)')
    export NAT_REGION_2_IP_ADDR=$(gcloud compute addresses describe ${SUBNET_2_REGION}-nat-ip --project=${HOST_NET_PROJECT}-${FOLDER_ID} --region=${SUBNET_2_REGION} --format='value(address)')
    export NAT_REGION_2_IP_NAME=$(gcloud compute addresses describe ${SUBNET_2_REGION}-nat-ip --project=${HOST_NET_PROJECT}-${FOLDER_ID} --region=${SUBNET_2_REGION} --format='value(name)')
    ```

1.  Create FW rule to allow traffic between Pods in all four clusters, and from the cluster control planes

    ```bash
    gcloud compute firewall-rules create all-pods-and-api-ipv4-cidrs \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --network ${SHARED_VPC} \
    --allow all \
    --direction INGRESS \
    --source-ranges 10.0.0.0/8,${GKE1_MASTER_IPV4_CIDR},${GKE2_MASTER_IPV4_CIDR},${GKE3_MASTER_IPV4_CIDR},${GKE4_MASTER_IPV4_CIDR}
    ```

1.  Create Cloud NAT Gateways in the required regions.

    ```bash
    gcloud compute routers create rtr-${SUBNET_1_REGION} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --network=${SHARED_VPC} \
    --region ${SUBNET_1_REGION}

    gcloud compute routers nats create nat-gw-${SUBNET_1_REGION} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --router=rtr-${SUBNET_1_REGION} \
    --region ${SUBNET_1_REGION} \
    --nat-external-ip-pool=${NAT_REGION_1_IP_NAME} \
    --nat-all-subnet-ip-ranges \
    --enable-logging

    gcloud compute routers create rtr-${SUBNET_2_REGION} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --network=${SHARED_VPC} \
    --region ${SUBNET_2_REGION}

    gcloud compute routers nats create nat-gw-${SUBNET_2_REGION} \
    --project ${HOST_NET_PROJECT}-${FOLDER_ID} \
    --router=rtr-${SUBNET_2_REGION} \
    --region ${SUBNET_2_REGION} \
    --nat-external-ip-pool=${NAT_REGION_2_IP_NAME} \
    --nat-all-subnet-ip-ranges \
    --enable-logging
    ```

## Create GKE clusters

In this section, you create four GKE private clusters. Two clusters are created in the `bank` project and two clusters are created in the `shop` project.

1.  Create two GKE clusters. In this workshop, we will be creating clusters with private nodes but publicly exposed control plane endpoints which will have access limited from master authorized networks.

    ```bash
    export MESH_ID=proj-${HOST_NET_PROJECT_NUM}
    gcloud beta container clusters create ${GKE1} \
    --project ${SVC_1_PROJECT}-${FOLDER_ID} \
    --zone=${GKE1_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --network projects/${HOST_NET_PROJECT}-${FOLDER_ID}/global/networks/${SHARED_VPC} \
    --subnetwork projects/${HOST_NET_PROJECT}-${FOLDER_ID}/regions/${SUBNET_1_REGION}/subnetworks/${SUBNET_1_NAME} \
    --cluster-secondary-range-name ${SUBNET_1_POD_NAME} \
    --services-secondary-range-name ${SUBNET_1_SVC_1_NAME} \
    --workload-pool=${SVC_1_PROJECT}-${FOLDER_ID}.svc.id.goog \
    --enable-private-nodes \
    --master-ipv4-cidr=${GKE1_MASTER_IPV4_CIDR} \
    --enable-master-authorized-networks \
    --master-authorized-networks $NAT_REGION_1_IP_ADDR/32,$NAT_REGION_2_IP_ADDR/32,$CLOUDSHELL_IP/32 \
    --labels=mesh_id=${MESH_ID} --async

    gcloud beta container clusters create ${GKE2} \
    --project ${SVC_1_PROJECT}-${FOLDER_ID} \
    --zone=${GKE2_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --network projects/${HOST_NET_PROJECT}-${FOLDER_ID}/global/networks/${SHARED_VPC} \
    --subnetwork projects/${HOST_NET_PROJECT}-${FOLDER_ID}/regions/${SUBNET_1_REGION}/subnetworks/${SUBNET_1_NAME} \
    --cluster-secondary-range-name ${SUBNET_1_POD_NAME} \
    --services-secondary-range-name ${SUBNET_1_SVC_2_NAME} \
    --workload-pool=${SVC_1_PROJECT}-${FOLDER_ID}.svc.id.goog \
    --enable-private-nodes \
    --master-ipv4-cidr=${GKE2_MASTER_IPV4_CIDR} \
    --enable-master-authorized-networks \
    --master-authorized-networks $NAT_REGION_1_IP_ADDR/32,$NAT_REGION_2_IP_ADDR/32,$CLOUDSHELL_IP/32 \
    --labels=mesh_id=${MESH_ID} --async

    gcloud beta container clusters create ${GKE3} \
    --project ${SVC_2_PROJECT}-${FOLDER_ID} \
    --zone=${GKE3_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --network projects/${HOST_NET_PROJECT}-${FOLDER_ID}/global/networks/${SHARED_VPC} \
    --subnetwork projects/${HOST_NET_PROJECT}-${FOLDER_ID}/regions/${SUBNET_2_REGION}/subnetworks/${SUBNET_2_NAME} \
    --cluster-secondary-range-name ${SUBNET_2_POD_NAME} \
    --services-secondary-range-name ${SUBNET_2_SVC_1_NAME} \
    --workload-pool=${SVC_2_PROJECT}-${FOLDER_ID}.svc.id.goog \
    --enable-private-nodes \
    --master-ipv4-cidr=${GKE3_MASTER_IPV4_CIDR} \
    --enable-master-authorized-networks \
    --master-authorized-networks $NAT_REGION_1_IP_ADDR/32,$NAT_REGION_2_IP_ADDR/32,$CLOUDSHELL_IP/32 \
    --labels=mesh_id=${MESH_ID} --async

    gcloud beta container clusters create ${GKE4} \
    --project ${SVC_2_PROJECT}-${FOLDER_ID} \
    --zone=${GKE4_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --network projects/${HOST_NET_PROJECT}-${FOLDER_ID}/global/networks/${SHARED_VPC} \
    --subnetwork projects/${HOST_NET_PROJECT}-${FOLDER_ID}/regions/${SUBNET_2_REGION}/subnetworks/${SUBNET_2_NAME} \
    --cluster-secondary-range-name ${SUBNET_2_POD_NAME} \
    --services-secondary-range-name ${SUBNET_2_SVC_2_NAME} \
    --workload-pool=${SVC_2_PROJECT}-${FOLDER_ID}.svc.id.goog \
    --enable-private-nodes \
    --master-ipv4-cidr=${GKE4_MASTER_IPV4_CIDR} \
    --enable-master-authorized-networks \
    --master-authorized-networks $NAT_REGION_1_IP_ADDR/32,$NAT_REGION_2_IP_ADDR/32,$CLOUDSHELL_IP/32 \
    --labels=mesh_id=${MESH_ID}
    ```

1.  Confirm clusters are `RUNNING` in service project 1.

    ```bash
    gcloud container clusters list --project ${SVC_1_PROJECT}-${FOLDER_ID}
    ```

The output is similar to the following:  

```
    NAME            LOCATION    MASTER_VERSION    MASTER_IP     MACHINE_TYPE   NODE_VERSION      NUM_NODES  STATUS
    gke-1-r1a-prod  us-west2-a  1.16.15-gke.6000  35.235.82.20  e2-standard-4  1.16.15-gke.6000  3          RUNNING
    gke-2-r1b-prod  us-west2-b  1.16.15-gke.6000  35.236.8.51   e2-standard-4  1.16.15-gke.6000  3          RUNNING
```

1.  Confirm clusters are `RUNNING` in service project 2.

    ```bash
    gcloud container clusters list --project ${SVC_2_PROJECT}-${FOLDER_ID}
    ```

The output is similar to the following:  

```
    NAME            LOCATION       MASTER_VERSION    MASTER_IP        MACHINE_TYPE   NODE_VERSION      NUM_NODES  STATUS
    gke-3-r2a-prod  us-central1-a  1.16.15-gke.6000  35.225.115.143   e2-standard-4  1.16.15-gke.6000  3          RUNNING
    gke-4-r2b-prod  us-central1-b  1.16.15-gke.6000  104.197.223.145  e2-standard-4  1.16.15-gke.6000  3          RUNNING
```

1.  Connect to clusters.

    ```bash
    touch asm-kubeconfig && export KUBECONFIG=`pwd`/asm-kubeconfig
    gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
    gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
    gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
    gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
    ```

Remember to unset your `KUBECONFIG` var at the end.

1.  Rename cluster context for easy switching.

    ```bash
    kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE1_ZONE}_${GKE1}
    kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE2_ZONE}_${GKE2}
    kubectl ctx ${GKE3}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE3_ZONE}_${GKE3}
    kubectl ctx ${GKE4}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE4_ZONE}_${GKE4}
    ```

1.  Confirm both cluster contextx are present.

    ```bash
    kubectl ctx
    ```

The output is similar to the following:  

```
    gke-1-r1a-prod
    gke-2-r1b-prod
    gke-3-r2a-prod
    gke-4-r2b-prod
```

## Register clusters to the host project and set up environ workload identity

In this section, you register all four clusters to an [environ host project](https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project). You use the network host project as the environ host project. You can use any project as the environ host project as long as all clusters are register in the same environ host project. 

1.  In the network host project, Open port 15017 in the firewall to get the webhook used with automatic sidecar injection to work properly.

    ```bash
    export GKE1_FW_NAME=$(gcloud compute firewall-rules list --filter="name~gke-${GKE1}-[0-9a-z]*-master" --project ${HOST_NET_PROJECT}-${FOLDER_ID} --format='value(name)')
    export GKE2_FW_NAME=$(gcloud compute firewall-rules list --filter="name~gke-${GKE2}-[0-9a-z]*-master" --project ${HOST_NET_PROJECT}-${FOLDER_ID} --format='value(name)')
    export GKE3_FW_NAME=$(gcloud compute firewall-rules list --filter="name~gke-${GKE3}-[0-9a-z]*-master" --project ${HOST_NET_PROJECT}-${FOLDER_ID} --format='value(name)')
    export GKE4_FW_NAME=$(gcloud compute firewall-rules list --filter="name~gke-${GKE4}-[0-9a-z]*-master" --project ${HOST_NET_PROJECT}-${FOLDER_ID} --format='value(name)')

    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} compute firewall-rules update ${GKE1_FW_NAME} --allow tcp:10250,tcp:443,tcp:15017,tcp:15014,tcp:8080
    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} compute firewall-rules update ${GKE2_FW_NAME} --allow tcp:10250,tcp:443,tcp:15017,tcp:15014,tcp:8080
    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} compute firewall-rules update ${GKE3_FW_NAME} --allow tcp:10250,tcp:443,tcp:15017,tcp:15014,tcp:8080
    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} compute firewall-rules update ${GKE4_FW_NAME} --allow tcp:10250,tcp:443,tcp:15017,tcp:15014,tcp:8080
    ```

1.  Register all clusters to the network host project using workload identity.

    ```bash
    gcloud beta services identity create --service=gkehub.googleapis.com --project=${HOST_NET_PROJECT}-${FOLDER_ID}

    gcloud projects add-iam-policy-binding "${HOST_NET_PROJECT}-${FOLDER_ID}" \
    --member "serviceAccount:service-${HOST_NET_PROJECT_NUM}@gcp-sa-gkehub.iam.gserviceaccount.com" \
    --role roles/gkehub.serviceAgent
    gcloud projects add-iam-policy-binding "${SVC_1_PROJECT}-${FOLDER_ID}" \
    --member "serviceAccount:service-${HOST_NET_PROJECT_NUM}@gcp-sa-gkehub.iam.gserviceaccount.com" \
    --role roles/gkehub.serviceAgent
    gcloud projects add-iam-policy-binding "${SVC_2_PROJECT}-${FOLDER_ID}" \
    --member "serviceAccount:service-${HOST_NET_PROJECT_NUM}@gcp-sa-gkehub.iam.gserviceaccount.com" \
    --role roles/gkehub.serviceAgent

    export GKE1_URI=$(gcloud --project ${SVC_1_PROJECT}-${FOLDER_ID} container clusters list --uri | grep ${GKE1})
    export GKE2_URI=$(gcloud --project ${SVC_1_PROJECT}-${FOLDER_ID} container clusters list --uri | grep ${GKE2})
    export GKE3_URI=$(gcloud --project ${SVC_2_PROJECT}-${FOLDER_ID} container clusters list --uri | grep ${GKE3})
    export GKE4_URI=$(gcloud --project ${SVC_2_PROJECT}-${FOLDER_ID} container clusters list --uri | grep ${GKE4})

    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} beta container hub memberships register ${GKE1} \
    --gke-uri=${GKE1_URI} \
    --enable-workload-identity

    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} beta container hub memberships register ${GKE2} \
    --gke-uri=${GKE2_URI} \
    --enable-workload-identity

    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} beta container hub memberships register ${GKE3} \
    --gke-uri=${GKE3_URI} \
    --enable-workload-identity

    gcloud --project ${HOST_NET_PROJECT}-${FOLDER_ID} beta container hub memberships register ${GKE4} \
    --gke-uri=${GKE4_URI} \
    --enable-workload-identity
    ```

## Install ASM

In this section, you install ASM on all four clusters in a multi primary mode. ASM controlplane is deployed to all four clusters with cross cluster service discovery to create a single logical service mesh spanning four clusters inside two service projects.

1.  Initialize the two service projects to ready them for installation. Among other things, this command creates a service account to let control plane components, such as the sidecar proxy, securely access your project's data and resources.

    ```bash
    curl --request POST \
    --header "Authorization: Bearer $(gcloud auth print-access-token)" \
    --data '' \
    https://meshconfig.googleapis.com/v1alpha1/projects/"${SVC_1_PROJECT}-${FOLDER_ID}":initialize

    curl --request POST \
    --header "Authorization: Bearer $(gcloud auth print-access-token)" \
    --data '' \
    https://meshconfig.googleapis.com/v1alpha1/projects/"${SVC_2_PROJECT}-${FOLDER_ID}":initialize
    ```

1.  Get ASM binaries.

    ```bash
    curl -LO https://storage.googleapis.com/gke-release/asm/istio-"${ASM_VERSION}"-linux-amd64.tar.gz
    tar xzf istio-"${ASM_VERSION}"-linux-amd64.tar.gz
    cd istio-"${ASM_VERSION}"
    export PATH=$PWD/bin:$PATH
    ```

1.  Ensure the correct version of `istioctl` CLI is in your PATH.

    ```bash
    istioctl version
    ```

The output is similar to the following:  

```
    no running Istio pods in "istio-system"
    1.8.1-asm.5
```

1.  Prepare ASM IstioOperator manifest for the four GKE clusters using kpt.

    ```bash
    kpt pkg get \
    https://github.com/GoogleCloudPlatform/anthos-service-mesh-packages.git/asm@release-"${ASM_MAJOR_VERSION}"-asm ${GKE1}

    kpt pkg get \
    https://github.com/GoogleCloudPlatform/anthos-service-mesh-packages.git/asm@release-"${ASM_MAJOR_VERSION}"-asm ${GKE2}

    kpt pkg get \
    https://github.com/GoogleCloudPlatform/anthos-service-mesh-packages.git/asm@release-"${ASM_MAJOR_VERSION}"-asm ${GKE3}

    kpt pkg get \
    https://github.com/GoogleCloudPlatform/anthos-service-mesh-packages.git/asm@release-"${ASM_MAJOR_VERSION}"-asm ${GKE4}
    ```

1.  Set ASM values for GKE1.

    ```bash
    kpt cfg set ${GKE1} gcloud.core.project ${SVC_1_PROJECT}-${FOLDER_ID}
    kpt cfg set ${GKE1} gcloud.project.environProjectNumber ${HOST_NET_PROJECT_NUM}
    kpt cfg set ${GKE1} gcloud.container.cluster ${GKE1}
    kpt cfg set ${GKE1} gcloud.compute.location ${GKE1_ZONE}
    kpt cfg set ${GKE1} anthos.servicemesh.rev ${ASM_LABEL}
    kpt cfg set ${GKE1} anthos.servicemesh.trustDomainAliases ${SVC_1_PROJECT}-${FOLDER_ID}.svc.id.goog ${SVC_2_PROJECT}-${FOLDER_ID}.svc.id.goog
    ```

1.  Verify ASM values for GKE1.

    ```bash
    kpt cfg list-setters ${GKE1}
    ```

The output is similar to the following:  

```
                             NAME                                                                  VALUE                                           SET BY   DESCRIPTION   COUNT   REQUIRED   IS SET  
      anthos.servicemesh.canonicalServiceHub               gcr.io/gke-release/asm/canonical-service-controller:1.7.3-asm.6                                                1       No         Yes     
      anthos.servicemesh.controlplane.monitoring.enabled   true                                                                                                           3       No         No      
      anthos.servicemesh.external_ca.ca_name                                                                                                                              1       No         Yes     
      anthos.servicemesh.hub                               gcr.io/gke-release/asm                                                                                         1       No         No      
      anthos.servicemesh.hubMembershipID                   MEMBERSHIP_ID                                                                                                  2       No         No      
      anthos.servicemesh.rev                               asm-181-5                                                                                                      2       No         Yes     
      anthos.servicemesh.tag                               1.8.1-asm.5                                                                                                    1       No         Yes     
      anthos.servicemesh.trustDomainAliases                [proj-1-bank-prod-814581352178.svc.id.goog,proj-2-shop-prod-814581352178.svc.id.goog]                          2       No         Yes     
      base-dir                                             base                                                                                                           0       No         No      
      gcloud.compute.location                              us-west2-a                                                                                                     16      No         Yes     
      gcloud.compute.network                               default                                                                                                        1       No         No      
      gcloud.compute.subnetwork                            default                                                                                                        1       No         No      
      gcloud.container.cluster                             gke-1-r1a-prod                                                                                                 17      No         Yes     
      gcloud.container.cluster.clusterSecondaryRange                                                                                                                      1       No         No      
      gcloud.container.cluster.ingress.httpPort            31224                                                                                                          1       No         Yes     
      gcloud.container.cluster.ingress.statusPort          31223                                                                                                          1       No         Yes     
      gcloud.container.cluster.releaseChannel              REGULAR                                                                                                        1       No         No      
      gcloud.container.cluster.servicesSecondaryRange                                                                                                                     1       No         No      
      gcloud.container.nodepool.max-nodes                  4                                                                                                              1       No         No      
      gcloud.core.project                                  proj-1-bank-prod-814581352178                                                                                  33      No         Yes     
      gcloud.project.environProjectID                      ENVIRON_PROJECT_ID                                                                                             6       No         No      
      gcloud.project.environProjectNumber                  448184974609                                                                                                   5       No         Yes     
      gcloud.project.projectNumber                         809253175928                                                                                                   0       No         Yes  
```   

1.  Install ASM on GKE1.

    ```bash
    istioctl --context ${GKE1} install \
    -f ${GKE1}/istio/istio-operator.yaml \
    -f ${GKE1}/istio/options/multiproject.yaml \
    -f ${GKE1}/istio/options/multicluster.yaml \
    --revision=asm-181-5 --skip-confirmation
    ```

1.  Set ASM values for GKE2.

    ```bash
    kpt cfg set ${GKE2} gcloud.core.project ${SVC_1_PROJECT}-${FOLDER_ID}
    kpt cfg set ${GKE2} gcloud.project.environProjectNumber ${HOST_NET_PROJECT_NUM}
    kpt cfg set ${GKE2} gcloud.container.cluster ${GKE2}
    kpt cfg set ${GKE2} gcloud.compute.location ${GKE2_ZONE}
    kpt cfg set ${GKE2} anthos.servicemesh.rev ${ASM_LABEL}
    kpt cfg set ${GKE2} anthos.servicemesh.trustDomainAliases ${SVC_1_PROJECT}-${FOLDER_ID}.svc.id.goog ${SVC_2_PROJECT}-${FOLDER_ID}.svc.id.goog
    ```

1.  Verify ASM values for GKE2.

    ```bash
    kpt cfg list-setters ${GKE2}
    ```

The output is similar to the following:  

```
                             NAME                                                                  VALUE                                           SET BY   DESCRIPTION   COUNT   REQUIRED   IS SET  
      anthos.servicemesh.canonicalServiceHub               gcr.io/gke-release/asm/canonical-service-controller:1.7.3-asm.6                                                1       No         Yes     
      anthos.servicemesh.controlplane.monitoring.enabled   true                                                                                                           3       No         No      
      anthos.servicemesh.external_ca.ca_name                                                                                                                              1       No         Yes     
      anthos.servicemesh.hub                               gcr.io/gke-release/asm                                                                                         1       No         No      
      anthos.servicemesh.hubMembershipID                   MEMBERSHIP_ID                                                                                                  2       No         No      
      anthos.servicemesh.rev                               asm-181-5                                                                                                      2       No         Yes     
      anthos.servicemesh.tag                               1.8.1-asm.5                                                                                                    1       No         Yes     
      anthos.servicemesh.trustDomainAliases                [proj-1-bank-prod-814581352178.svc.id.goog,proj-2-shop-prod-814581352178.svc.id.goog]                          2       No         Yes     
      base-dir                                             base                                                                                                           0       No         No      
      gcloud.compute.location                              us-west2-b                                                                                                     16      No         Yes     
      gcloud.compute.network                               default                                                                                                        1       No         No      
      gcloud.compute.subnetwork                            default                                                                                                        1       No         No      
      gcloud.container.cluster                             gke-2-r1b-prod                                                                                                 17      No         Yes     
      gcloud.container.cluster.clusterSecondaryRange                                                                                                                      1       No         No      
      gcloud.container.cluster.ingress.httpPort            31224                                                                                                          1       No         Yes     
      gcloud.container.cluster.ingress.statusPort          31223                                                                                                          1       No         Yes     
      gcloud.container.cluster.releaseChannel              REGULAR                                                                                                        1       No         No      
      gcloud.container.cluster.servicesSecondaryRange                                                                                                                     1       No         No      
      gcloud.container.nodepool.max-nodes                  4                                                                                                              1       No         No      
      gcloud.core.project                                  proj-1-bank-prod-814581352178                                                                                  33      No         Yes     
      gcloud.project.environProjectID                      ENVIRON_PROJECT_ID                                                                                             6       No         No      
      gcloud.project.environProjectNumber                  448184974609                                                                                                   5       No         Yes     
      gcloud.project.projectNumber                         809253175928                                                                                                   0       No         Yes  
```   

1.  Install ASM on GKE2.

    ```bash
    istioctl --context ${GKE2} install \
    -f ${GKE2}/istio/istio-operator.yaml \
    -f ${GKE2}/istio/options/multiproject.yaml \
    -f ${GKE2}/istio/options/multicluster.yaml \
    --revision=asm-181-5 --skip-confirmation
    ```

1.  Set ASM values for GKE3.

    ```bash
    kpt cfg set ${GKE3} gcloud.core.project ${SVC_2_PROJECT}-${FOLDER_ID}
    kpt cfg set ${GKE3} gcloud.project.environProjectNumber ${HOST_NET_PROJECT_NUM}
    kpt cfg set ${GKE3} gcloud.container.cluster ${GKE3}
    kpt cfg set ${GKE3} gcloud.compute.location ${GKE3_ZONE}
    kpt cfg set ${GKE3} anthos.servicemesh.rev ${ASM_LABEL}
    kpt cfg set ${GKE3} anthos.servicemesh.trustDomainAliases ${SVC_1_PROJECT}-${FOLDER_ID}.svc.id.goog ${SVC_2_PROJECT}-${FOLDER_ID}.svc.id.goog
    ```

1.  Verify ASM values for GKE3.

    ```bash
    kpt cfg list-setters ${GKE3}
    ```

The output is similar to the following:  

```
                             NAME                                                                  VALUE                                           SET BY   DESCRIPTION   COUNT   REQUIRED   IS SET  
      anthos.servicemesh.canonicalServiceHub               gcr.io/gke-release/asm/canonical-service-controller:1.7.3-asm.6                                                1       No         Yes     
      anthos.servicemesh.controlplane.monitoring.enabled   true                                                                                                           3       No         No      
      anthos.servicemesh.external_ca.ca_name                                                                                                                              1       No         Yes     
      anthos.servicemesh.hub                               gcr.io/gke-release/asm                                                                                         1       No         No      
      anthos.servicemesh.hubMembershipID                   MEMBERSHIP_ID                                                                                                  2       No         No      
      anthos.servicemesh.rev                               asm-181-5                                                                                                      2       No         Yes     
      anthos.servicemesh.tag                               1.8.1-asm.5                                                                                                    1       No         Yes     
      anthos.servicemesh.trustDomainAliases                [proj-1-bank-prod-814581352178.svc.id.goog,proj-2-shop-prod-814581352178.svc.id.goog]                          2       No         Yes     
      base-dir                                             base                                                                                                           0       No         No      
      gcloud.compute.location                              us-central1-a                                                                                                  16      No         Yes     
      gcloud.compute.network                               default                                                                                                        1       No         No      
      gcloud.compute.subnetwork                            default                                                                                                        1       No         No      
      gcloud.container.cluster                             gke-3-r2a-prod                                                                                                 17      No         Yes     
      gcloud.container.cluster.clusterSecondaryRange                                                                                                                      1       No         No      
      gcloud.container.cluster.ingress.httpPort            31224                                                                                                          1       No         Yes     
      gcloud.container.cluster.ingress.statusPort          31223                                                                                                          1       No         Yes     
      gcloud.container.cluster.releaseChannel              REGULAR                                                                                                        1       No         No      
      gcloud.container.cluster.servicesSecondaryRange                                                                                                                     1       No         No      
      gcloud.container.nodepool.max-nodes                  4                                                                                                              1       No         No      
      gcloud.core.project                                  proj-2-shop-prod-814581352178                                                                                  33      No         Yes     
      gcloud.project.environProjectID                      ENVIRON_PROJECT_ID                                                                                             6       No         No      
      gcloud.project.environProjectNumber                  448184974609                                                                                                   5       No         Yes     
      gcloud.project.projectNumber                         866436631067                                                                                                   0       No         Yes  
```   

1.  Install ASM on GKE3.

    ```bash
    istioctl --context ${GKE3} install \
    -f ${GKE3}/istio/istio-operator.yaml \
    -f ${GKE3}/istio/options/multiproject.yaml \
    -f ${GKE3}/istio/options/multicluster.yaml \
    --revision=asm-181-5 --skip-confirmation
    ```

1.  Set ASM values for GKE4.

    ```bash
    kpt cfg set ${GKE4} gcloud.core.project ${SVC_2_PROJECT}-${FOLDER_ID}
    kpt cfg set ${GKE4} gcloud.project.environProjectNumber ${HOST_NET_PROJECT_NUM}
    kpt cfg set ${GKE4} gcloud.container.cluster ${GKE4}
    kpt cfg set ${GKE4} gcloud.compute.location ${GKE4_ZONE}
    kpt cfg set ${GKE4} anthos.servicemesh.rev ${ASM_LABEL}
    kpt cfg set ${GKE4} anthos.servicemesh.trustDomainAliases ${SVC_1_PROJECT}-${FOLDER_ID}.svc.id.goog ${SVC_2_PROJECT}-${FOLDER_ID}.svc.id.goog
    ```

1.  Verify ASM values for GKE4.

    ```bash
    kpt cfg list-setters ${GKE4}
    ```

The output is similar to the following:  

```
                             NAME                                                                  VALUE                                           SET BY   DESCRIPTION   COUNT   REQUIRED   IS SET  
      anthos.servicemesh.canonicalServiceHub               gcr.io/gke-release/asm/canonical-service-controller:1.7.3-asm.6                                                1       No         Yes     
      anthos.servicemesh.controlplane.monitoring.enabled   true                                                                                                           3       No         No      
      anthos.servicemesh.external_ca.ca_name                                                                                                                              1       No         Yes     
      anthos.servicemesh.hub                               gcr.io/gke-release/asm                                                                                         1       No         No      
      anthos.servicemesh.hubMembershipID                   MEMBERSHIP_ID                                                                                                  2       No         No      
      anthos.servicemesh.rev                               asm-181-5                                                                                                      2       No         Yes     
      anthos.servicemesh.tag                               1.8.1-asm.5                                                                                                    1       No         Yes     
      anthos.servicemesh.trustDomainAliases                [proj-1-bank-prod-814581352178.svc.id.goog,proj-2-shop-prod-814581352178.svc.id.goog]                          2       No         Yes     
      base-dir                                             base                                                                                                           0       No         No      
      gcloud.compute.location                              us-central1-b                                                                                                  16      No         Yes     
      gcloud.compute.network                               default                                                                                                        1       No         No      
      gcloud.compute.subnetwork                            default                                                                                                        1       No         No      
      gcloud.container.cluster                             gke-4-r2b-prod                                                                                                 17      No         Yes     
      gcloud.container.cluster.clusterSecondaryRange                                                                                                                      1       No         No      
      gcloud.container.cluster.ingress.httpPort            31224                                                                                                          1       No         Yes     
      gcloud.container.cluster.ingress.statusPort          31223                                                                                                          1       No         Yes     
      gcloud.container.cluster.releaseChannel              REGULAR                                                                                                        1       No         No      
      gcloud.container.cluster.servicesSecondaryRange                                                                                                                     1       No         No      
      gcloud.container.nodepool.max-nodes                  4                                                                                                              1       No         No      
      gcloud.core.project                                  proj-2-shop-prod-814581352178                                                                                  33      No         Yes     
      gcloud.project.environProjectID                      ENVIRON_PROJECT_ID                                                                                             6       No         No      
      gcloud.project.environProjectNumber                  448184974609                                                                                                   5       No         Yes     
      gcloud.project.projectNumber                         866436631067                                                                                                   0       No         Yes    
``` 

1.  Install ASM on GKE4.

    ```bash
    istioctl --context ${GKE4} install \
    -f ${GKE4}/istio/istio-operator.yaml \
    -f ${GKE4}/istio/options/multiproject.yaml \
    -f ${GKE4}/istio/options/multicluster.yaml \
    --revision=asm-181-5 --skip-confirmation
    ```

### Enable multi cluster service discovery

1.  Create a secret manifest for all clusters' kubeconfig.

    ```bash
    istioctl x create-remote-secret \
    --context=${GKE1} \
    --name=${GKE1} > secret-kubeconfig-${GKE1}.yaml

    istioctl x create-remote-secret \
    --context=${GKE2} \
    --name=${GKE2} > secret-kubeconfig-${GKE2}.yaml

    istioctl x create-remote-secret \
    --context=${GKE3} \
    --name=${GKE3} > secret-kubeconfig-${GKE3}.yaml

    istioctl x create-remote-secret \
    --context=${GKE4} \
    --name=${GKE4} > secret-kubeconfig-${GKE4}.yaml
    ```

1.  Create secrets in all clusters. Every cluster must have a secret for the other three clusters. You do not need to create a secret for the local cluster's kubeconfig.

    ```bash
    kubectl --context=${GKE1} -n istio-system apply -f secret-kubeconfig-${GKE2}.yaml
    kubectl --context=${GKE1} -n istio-system apply -f secret-kubeconfig-${GKE3}.yaml
    kubectl --context=${GKE1} -n istio-system apply -f secret-kubeconfig-${GKE4}.yaml

    kubectl --context=${GKE2} -n istio-system apply -f secret-kubeconfig-${GKE1}.yaml
    kubectl --context=${GKE2} -n istio-system apply -f secret-kubeconfig-${GKE3}.yaml
    kubectl --context=${GKE2} -n istio-system apply -f secret-kubeconfig-${GKE4}.yaml

    kubectl --context=${GKE3} -n istio-system apply -f secret-kubeconfig-${GKE1}.yaml
    kubectl --context=${GKE3} -n istio-system apply -f secret-kubeconfig-${GKE2}.yaml
    kubectl --context=${GKE3} -n istio-system apply -f secret-kubeconfig-${GKE4}.yaml

    kubectl --context=${GKE4} -n istio-system apply -f secret-kubeconfig-${GKE1}.yaml
    kubectl --context=${GKE4} -n istio-system apply -f secret-kubeconfig-${GKE2}.yaml
    kubectl --context=${GKE4} -n istio-system apply -f secret-kubeconfig-${GKE3}.yaml
    ```

### Verify cross cluster service mesh

1.  Create a sample namespace and a helloworld service in all four clusters.

    ```bash
    for CTX in ${GKE1} ${GKE2} ${GKE3} ${GKE4}
    do
      kubectl create --context=${CTX} namespace sample
      kubectl label --context=${CTX} namespace sample \
        istio.io/rev=${ASM_LABEL}
      kubectl create --context=${CTX} \
        -f samples/helloworld/helloworld.yaml \
        -l app=helloworld -l service=helloworld -n sample
    done
    ```

1.  Create helloworld-v1 deployments in GKE1 and GKE3 clusters.

    ```bash
    for CTX in ${GKE1} ${GKE3}
    do
      kubectl create --context=${CTX} \
        -f samples/helloworld/helloworld.yaml \
        -l app=helloworld -l version=v1 -n sample
    done
    ```

1.  Create helloworld-v2 deployments in GKE2 and GKE4 clusters.

    ```bash
    for CTX in ${GKE2} ${GKE4}
    do
      kubectl create --context=${CTX} \
        -f samples/helloworld/helloworld.yaml \
        -l app=helloworld -l version=v2 -n sample
    done
    ```

1.  Deploy sleep service in all clusters in the sample namespace.

    ```bash
    for CTX in ${GKE1} ${GKE2} ${GKE3} ${GKE4}
    do
      kubectl apply --context=${CTX} \
        -f samples/sleep/sleep.yaml -n sample
    done
    ```

1.  Get sleep pods from all clusters.

    ```bash
    export GKE1_SLEEP_POD=$(kubectl get pod -n sample -l app=sleep --context=${GKE1} -o jsonpath='{.items[0].metadata.name}')
    export GKE2_SLEEP_POD=$(kubectl get pod -n sample -l app=sleep --context=${GKE2} -o jsonpath='{.items[0].metadata.name}')
    export GKE3_SLEEP_POD=$(kubectl get pod -n sample -l app=sleep --context=${GKE3} -o jsonpath='{.items[0].metadata.name}')
    export GKE4_SLEEP_POD=$(kubectl get pod -n sample -l app=sleep --context=${GKE4} -o jsonpath='{.items[0].metadata.name}')
    ```

1.  Check proxy configs. You should see four endpoints for the `helloworld` service, one is each cluster. If you get a … `container not found ("istio-proxy")`, error it may take some time for the sleep pods to come up.

    ```bash
    istioctl --context $GKE1 -n sample pc ep $GKE1_SLEEP_POD | grep helloworld
    istioctl --context $GKE2 -n sample pc ep $GKE2_SLEEP_POD | grep helloworld
    istioctl --context $GKE3 -n sample pc ep $GKE3_SLEEP_POD | grep helloworld
    istioctl --context $GKE4 -n sample pc ep $GKE4_SLEEP_POD | grep helloworld
    ```

The output is similar to the following:  

```
    10.0.1.5:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.3.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.2.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.5.7:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.1.5:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.3.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.2.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.5.7:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.1.5:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.3.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.2.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.5.7:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.1.5:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.0.3.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.2.6:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
    10.8.5.7:5000                    HEALTHY     OK                outbound|5000||helloworld.sample.svc.cluster.local
```

1.  Test connectivity from GKE1. Curl the `helloworld.sample` Service multiple times from the `sleep` pod in GKE1. You see responses from Pods in all four clusters (v1 are the Pods in GKE1 and GKE3 and v2 are the Pods in GKE2 and GKE4)

    ```bash
    # from GKE1
    for i in {1..15}
      do kubectl exec -it -n sample -c sleep --context=${GKE1} $GKE1_SLEEP_POD -- curl helloworld.sample:5000/hello
    done
    ```

1.  Test connectivity from GKE2.

    ```bash
    # from GKE2
    for i in {1..15}
      do kubectl exec -it -n sample -c sleep --context=${GKE2} $GKE2_SLEEP_POD -- curl helloworld.sample:5000/hello
    done
    ```

1.  Test connectivity from GKE3.

    ```bash
    # from GKE3
    for i in {1..15}
      do kubectl exec -it -n sample -c sleep --context=${GKE3} $GKE3_SLEEP_POD -- curl helloworld.sample:5000/hello
    done
    ```

1.  Test connectivity from GKE3.

    ```bash
    # from GKE4
    for i in {1..15}
      do kubectl exec -it -n sample -c sleep --context=${GKE4} $GKE4_SLEEP_POD -- curl helloworld.sample:5000/hello
    done
    ```

## Bank of Anthos

In this section, you deploy Bank of Anthos to GKE1 and GKE2 clusters in the `bank` service project.  

1.  Clone the Bank of Anthos repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/bank-of-anthos.git bank-of-anthos
    ```

1.  Create and label `bank-of-anthos` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${GKE1} namespace bank-of-anthos
    kubectl label --context=${GKE1} namespace bank-of-anthos istio.io/rev=${ASM_LABEL}
    kubectl create --context=${GKE2} namespace bank-of-anthos
    kubectl label --context=${GKE2} namespace bank-of-anthos istio.io/rev=${ASM_LABEL}
    ```

1.  Deploy Bank of Anthos application to both cluisters in the `bank-of-anthos` namespace. Deploy the two PostGres DBs only in one of two clusters by deleting the StatefulSets from the other cluster.

    ```bash
    kubectl --context=$GKE1 -n bank-of-anthos apply -f bank-of-anthos/extras/jwt/jwt-secret.yaml
    kubectl --context=$GKE2 -n bank-of-anthos apply -f bank-of-anthos/extras/jwt/jwt-secret.yaml
    kubectl --context=$GKE1 -n bank-of-anthos apply -f bank-of-anthos/kubernetes-manifests
    kubectl --context=$GKE2 -n bank-of-anthos apply -f bank-of-anthos/kubernetes-manifests
    kubectl --context=$GKE2 -n bank-of-anthos delete statefulset accounts-db
    kubectl --context=$GKE2 -n bank-of-anthos delete statefulset ledger-db
    ```

All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `accounts-db` and `ledger-db` is only deployed to GKE1.  

Verify that all Pods are running in both clusters.

1.  Get Pods from GKE1

    ```bash
    kubectl --context=${GKE1} -n bank-of-anthos get pod
    ```

1.  Get Pods from GKE2

    ```bash
    kubectl --context=${GKE2} -n bank-of-anthos get pod
    ```

The output is similar to the following:  

```
    # GKE1
    NAME                                  READY   STATUS    RESTARTS   AGE
    accounts-db-0                         2/2     Running   0          2m40s
    balancereader-c5d664b4c-tf8fg         2/2     Running   0          2m40s
    contacts-7fd8c5fb6-vprp9              2/2     Running   1          2m39s
    frontend-7b7fb9b665-g9wzd             2/2     Running   1          2m39s
    ledger-db-0                           2/2     Running   0          2m38s
    ledgerwriter-7b5b6db66f-2hsqt         2/2     Running   0          2m38s
    loadgenerator-7fb54d57f8-85bdc        2/2     Running   0          2m37s
    transactionhistory-7fdb998c5f-ns29r   2/2     Running   0          2m37s
    userservice-76996974f5-tv56x          2/2     Running   0          2m37s

    # GKE2
    NAME                                  READY   STATUS    RESTARTS   AGE
    balancereader-c5d664b4c-vwjrw         2/2     Running   1          2m37s
    contacts-7fd8c5fb6-5c2l9              2/2     Running   1          2m36s
    frontend-7b7fb9b665-gjllh             2/2     Running   1          2m36s
    ledgerwriter-7b5b6db66f-lp2vn         2/2     Running   0          2m35s
    loadgenerator-7fb54d57f8-wpsct        2/2     Running   0          2m34s
    transactionhistory-7fdb998c5f-8ndqx   2/2     Running   0          2m34s
    userservice-76996974f5-n6xwp          2/2     Running   1          2m34s
```

1.  Deploy the ASM configs to both clusters.

    ```bash
    kubectl --context=${GKE1} -n bank-of-anthos apply -f bank-of-anthos/istio-manifests
    kubectl --context=${GKE2} -n bank-of-anthos apply -f bank-of-anthos/istio-manifests
    ```

1.  Get the frontend Pod from GKE1.

    ```bash
    export BANK_GKE1_FRONTEND_POD=$(kubectl get pod -n bank-of-anthos -l app=frontend --context=${GKE1} -o jsonpath='{.items[0].metadata.name}')
    ```

1.  Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    istioctl --context $GKE1 -n bank-of-anthos pc ep $BANK_GKE1_FRONTEND_POD | grep bank-of-anthos
    ```

The output is similar to the following:  

```
    10.0.0.10:8080                   HEALTHY     OK                outbound|80||frontend.bank-of-anthos.svc.cluster.local
    10.0.0.11:8080                   HEALTHY     OK                outbound|8080||userservice.bank-of-anthos.svc.cluster.local
    10.0.1.6:5432                    HEALTHY     OK                outbound|5432||accounts-db.bank-of-anthos.svc.cluster.local
    10.0.1.7:8080                    HEALTHY     OK                outbound|8080||contacts.bank-of-anthos.svc.cluster.local
    10.0.2.10:8080                   HEALTHY     OK                outbound|8080||ledgerwriter.bank-of-anthos.svc.cluster.local
    10.0.2.12:8080                   HEALTHY     OK                outbound|8080||transactionhistory.bank-of-anthos.svc.cluster.local
    10.0.2.8:8080                    HEALTHY     OK                outbound|8080||balancereader.bank-of-anthos.svc.cluster.local
    10.0.2.9:5432                    HEALTHY     OK                outbound|5432||ledger-db.bank-of-anthos.svc.cluster.local
    10.0.3.7:8080                    HEALTHY     OK                outbound|8080||balancereader.bank-of-anthos.svc.cluster.local
    10.0.3.8:8080                    HEALTHY     OK                outbound|8080||contacts.bank-of-anthos.svc.cluster.local
    10.0.3.9:8080                    HEALTHY     OK                outbound|8080||userservice.bank-of-anthos.svc.cluster.local
    10.0.4.11:8080                   HEALTHY     OK                outbound|8080||ledgerwriter.bank-of-anthos.svc.cluster.local
    10.0.4.12:8080                   HEALTHY     OK                outbound|8080||transactionhistory.bank-of-anthos.svc.cluster.local
    10.0.5.8:8080                    HEALTHY     OK                outbound|80||frontend.bank-of-anthos.svc.cluster.local
```

With the exception of `accounts-db` and `ledger-db`, all other Services have two Endpoints. One Pod in each cluster.   

1.  Frontend service in both GKE1 and GKE2 clusters is exposed using `istio-ingressgateway` (using Gateway and VirtualService). You can access Bank of Anthos by navigating to the `istio-ingressgateway` public IP address from either of the two clusters.

    ```bash
    kubectl --context ${GKE1} --namespace istio-system get svc istio-ingressgateway -o jsonpath={.status.loadBalancer.ingress..ip}
    kubectl --context ${GKE2} --namespace istio-system get svc istio-ingressgateway -o jsonpath={.status.loadBalancer.ingress..ip}
    ```

The output is similar to the following:  

```
    # GKE1
    34.94.88.36

    # GKE2
    35.236.122.112
```

Access the application, login, create a deposit and transfer funds to other accounts. You can also create a new user and login using the new user and perform transactions.  
All features/services of the applications should be functional.  

## Online Boutique

In this section, you deploy Online Boutique to GKE3 and GKE4 clusters in the `shop` project.  

1.  Clone the Online Boutique repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/microservices-demo online-boutique
    ```

1.  Create and label `online-boutique` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${GKE3} namespace online-boutique
    kubectl label --context=${GKE3} namespace online-boutique istio.io/rev=${ASM_LABEL}
    kubectl create --context=${GKE4} namespace online-boutique
    kubectl label --context=${GKE4} namespace online-boutique istio.io/rev=${ASM_LABEL}
    ```

1.  Deploy Online Boutique to both cluisters in the `online-boutique` namespace. Deploy redis-cart Deployment only in one of two clusters. Redis is used by cartservice to store items in your shopping cart. It is the only stateful service in Online Boutique.

    ```bash
    kubectl --context=$GKE3 -n online-boutique apply -f online-boutique/release
    kubectl --context=$GKE4 -n online-boutique apply -f online-boutique/release
    kubectl --context=$GKE4 -n online-boutique delete deployment redis-cart
    ```

All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `redis-cart` is only deployed to GKE3.  

Verify that all Pods are running in both clusters.

1.  Get Pods from GKE3

    ```bash
    kubectl --context=${GKE3} -n online-boutique get pod
    ```

1.  Get Pods from GKE4

    ```bash
    kubectl --context=${GKE4} -n online-boutique get pod
    ```

The output is similar to the following:  

```
    # GKE3
    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-76bdd69666-zcsjk               2/2     Running   0          3m12s
    cartservice-66d497c6b7-5vgr2             2/2     Running   1          3m14s
    checkoutservice-666c784bd6-tsx9k         2/2     Running   0          3m15s
    currencyservice-5d5d496984-ghfs5         2/2     Running   0          3m13s
    emailservice-667457d9d6-7zn6m            2/2     Running   0          3m15s
    frontend-6b8d69b9fb-mgx9w                2/2     Running   0          3m15s
    loadgenerator-665b5cd444-l2nrf           2/2     Running   2          3m13s
    paymentservice-68596d6dd6-b47lq          2/2     Running   0          3m14s
    productcatalogservice-557d474574-bs6p5   2/2     Running   0          3m14s
    recommendationservice-69c56b74d4-9zk7l   2/2     Running   0          3m15s
    redis-cart-5f59546cdd-bgpvh              2/2     Running   0          3m12s
    shippingservice-6ccc89f8fd-v6q7l         2/2     Running   0          3m13s

    # GKE4
    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-76bdd69666-s7t79               2/2     Running   0          3m6s
    cartservice-66d497c6b7-5lpwp             2/2     Running   1          3m7s
    checkoutservice-666c784bd6-9vwb4         2/2     Running   0          3m9s
    currencyservice-5d5d496984-62fbr         2/2     Running   0          3m7s
    emailservice-667457d9d6-t95b5            2/2     Running   0          3m9s
    frontend-6b8d69b9fb-xppk2                2/2     Running   0          3m9s
    loadgenerator-665b5cd444-98hpm           2/2     Running   2          3m7s
    paymentservice-68596d6dd6-f2jvp          2/2     Running   0          3m8s
    productcatalogservice-557d474574-wqndp   2/2     Running   0          3m8s
    recommendationservice-69c56b74d4-vwt79   2/2     Running   0          3m9s
    shippingservice-6ccc89f8fd-rlhcc         2/2     Running   0          3m7
```

1.  Get the frontend Pod from GKE3.

    ```bash
    export SHOP_GKE3_FRONTEND_POD=$(kubectl get pod -n online-boutique -l app=frontend --context=${GKE3} -o jsonpath='{.items[0].metadata.name}')
    ```

1.  Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    istioctl --context $GKE3 -n online-boutique pc ep $SHOP_GKE3_FRONTEND_POD | grep online-boutique
    ```

The output is similar to the following:  

```
    10.8.0.7:50051                   HEALTHY     OK                outbound|50051||paymentservice.online-boutique.svc.cluster.local
    10.8.0.8:7070                    HEALTHY     OK                outbound|7070||cartservice.online-boutique.svc.cluster.local
    10.8.0.9:6379                    HEALTHY     OK                outbound|6379||redis-cart.online-boutique.svc.cluster.local
    10.8.1.10:8080                   HEALTHY     OK                outbound|8080||recommendationservice.online-boutique.svc.cluster.local
    10.8.1.11:5050                   HEALTHY     OK                outbound|5050||checkoutservice.online-boutique.svc.cluster.local
    10.8.1.13:7000                   HEALTHY     OK                outbound|7000||currencyservice.online-boutique.svc.cluster.local
    10.8.2.10:50051                  HEALTHY     OK                outbound|50051||shippingservice.online-boutique.svc.cluster.local
    10.8.2.11:9555                   HEALTHY     OK                outbound|9555||adservice.online-boutique.svc.cluster.local
    10.8.2.7:8080                    HEALTHY     OK                outbound|80||frontend-external.online-boutique.svc.cluster.local
    10.8.2.7:8080                    HEALTHY     OK                outbound|80||frontend.online-boutique.svc.cluster.local
    10.8.2.8:8080                    HEALTHY     OK                outbound|5000||emailservice.online-boutique.svc.cluster.local
    10.8.2.9:3550                    HEALTHY     OK                outbound|3550||productcatalogservice.online-boutique.svc.cluster.local
    10.8.3.5:8080                    HEALTHY     OK                outbound|8080||recommendationservice.online-boutique.svc.cluster.local
    10.8.3.6:50051                   HEALTHY     OK                outbound|50051||paymentservice.online-boutique.svc.cluster.local
    10.8.3.7:5050                    HEALTHY     OK                outbound|5050||checkoutservice.online-boutique.svc.cluster.local
    10.8.3.8:7070                    HEALTHY     OK                outbound|7070||cartservice.online-boutique.svc.cluster.local
    10.8.3.9:7000                    HEALTHY     OK                outbound|7000||currencyservice.online-boutique.svc.cluster.local
    10.8.4.9:50051                   HEALTHY     OK                outbound|50051||shippingservice.online-boutique.svc.cluster.local
    10.8.5.10:8080                   HEALTHY     OK                outbound|80||frontend-external.online-boutique.svc.cluster.local
    10.8.5.10:8080                   HEALTHY     OK                outbound|80||frontend.online-boutique.svc.cluster.local
    10.8.5.11:8080                   HEALTHY     OK                outbound|5000||emailservice.online-boutique.svc.cluster.local
    10.8.5.13:9555                   HEALTHY     OK                outbound|9555||adservice.online-boutique.svc.cluster.local
    10.8.5.9:3550                    HEALTHY     OK                outbound|3550||productcatalogservice.online-boutique.svc.cluster.local
```

With the exception of `redis-cart`, all other Services have two Endpoints. One Pod in each cluster.   

1.  Frontend service in both GKE3 and GKE4 clusters is exposed using istio-ingressgateway (using Gateway and VirtualService). You can access Online Boutique by navigating to the istio-ingressgateway public IP address from either of the two clusters.

    ```bash
    kubectl --context ${GKE3} --namespace istio-system get svc istio-ingressgateway -o jsonpath={.status.loadBalancer.ingress..ip}
    kubectl --context ${GKE4} --namespace istio-system get svc istio-ingressgateway -o jsonpath={.status.loadBalancer.ingress..ip}
    ```

The output is similar to the following:  

```
    # GKE3
    35.202.186.174

    # GKE4
    35.226.109.88
```

Access the application, browse through items, add them to your cart and checkout.  
All features/services of the applications should be functional.  

## Cleaning up

To avoid incurring charges to your Google Cloud Platform account for the resources used in this tutorial:  
You can delete the three projects and the folder you created for this guide.  
When you create a shared VPC, a lien is placed on the network host project. The lien must be removed prior to deleting the network host project. The two service projects can be deleted.

1.  Delete the two service projects. You can undelete these projects within 30 days of deletion.

    ```bash
    gcloud projects delete "${SVC_1_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_2_PROJECT}-${FOLDER_ID}" --quiet
    ```

The output is similar to the following:  

```
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-1-bank-prod-814581352178].
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-2-shop-prod-814581352178].
```

1.  Remove the lien from the network host project.

    ```bash
    export LIEN_ID=$(gcloud alpha resource-manager liens list --project="${HOST_NET_PROJECT}-${FOLDER_ID}" | awk 'NR==2 {print $1}')

    gcloud alpha resource-manager liens delete $LIEN_ID
    ```

The output is similar to the following:  

```
    Deleted [liens/p448184974609-lca24ae52-9480-44e9-877d-db8dd7020ad7].
```

1.  Delete the network host project.

    ```bash
    gcloud projects delete "${HOST_NET_PROJECT}-${FOLDER_ID}" --quiet
    ```

The output is similar to the following:  

```
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-0-net-prod-814581352178].
```

1.  Delete the folder

    ```bash
    gcloud resource-manager folders delete ${FOLDER_ID}
    ```

The output is similar to the following:  

```
    Deleted [<Folder
     createTime: '2021-01-29T21:58:38.290Z'
     displayName: 'asm18-shared-vpc'
     lifecycleState: LifecycleStateValueValuesEnum(DELETE_REQUESTED, 2)
     name: 'folders/814581352178'
     parent: 'organizations/542306964273'>].
```

1.  Unset `KUBECONFIG`.

    ```bash
    unset KUBECONFIG
    ```
    
## Appendix

Documentation used for this guide.

+   https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-shared-vpc
+   https://cloud.google.com/anthos/multicluster-management/connect/prerequisites
+   https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
+   https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project
+   https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm
+   https://cloud.google.com/kubernetes-engine/docs/how-to/troubleshooting-and-ops#avmbr110_iam_permission_permission_missing_for_cluster_name

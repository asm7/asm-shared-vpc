# Setting up ASM service mesh across multiple service projects on a shared VPC and private clusters using a Gitlab pipeline with Kubernetes Config Connector

&nbsp;
<img src="/docs/arch.svg" width=100% height=100%>
&nbsp;
&nbsp;
<img src="/docs/infra-complete-pipeline.png" width=100% height=100%>
&nbsp;

#### Table of contents

[[_TOC_]]

This tutorial shows how to run distributed services on multiple GKE clusters in multiple service projects on a shared VPC in GCP using Anthos Service Mesh (ASM). A distributed service is a Kubernetes Service that runs on multiple Kubernetes clusters in the same namespace and acts as a single logical service. Distributed services are more resilient than Kubernetes Services because they run on multiple GKE clusters. A distributed service remains up even if one or more GKE clusters are down, as long as the healthy clusters are able to serve the desired load.  
Kubernetes Services are known only to the Kubernetes API server of the cluster they run on. If the Kubernetes cluster is down (for example during a scheduled maintenance), so are all of the Kubernetes Services running on that cluster. Running distributed services makes cluster lifecycle management easier since clusters can be brought down for maintenance or upgrades while other cluster service service traffic. In order to create a distributed service, service mesh functionality provided by ASM is used to "glue" services running on multiple clusters together to act as a single logical service.  
This guide shows how to set up a shared VPC with multiple service projects. Then, how to create multiple GKE private clusters in multiple service projects, how to deploy ASM on multiple GKE private clusters and how to run distributed services on this platform. You may use the same guide for non-private GKE clusters as well. The configuration intended strictly for private clusters is highlighted in this guide.  
This guide is intended for platform administrators and service operators with basic knowledge of Kubernetes. Some knowledge of service mesh is beneficial although not necessary. ASM is based on the OSS Istio technology. You can learn more about service mesh and Istio on [istio.io](istio.io).

## Kubernetes Config Connector

This guide primarily uses [Kubernetes Config Connector](https://cloud.google.com/config-connector/docs/overview) for infrastructure resource management in GCP. Config Connector is a Kubernetes add-on that allows you to manage Google Cloud resources through Kubernetes. You use declarative Kubernetes manifests to deploy GCP resources for example projects, networks and GKE clusters. The configurations in this guide can be used in an infrastructure deployment pipeline. All configs can be stored in a Git repo and can follow the typical Gitops methodology for infrastructure deployment.  
In this guide, you create a folder for KCC and a project called `proj-infra-admin` inside that folder. This project contains a single GKE cluster called `kcc` with the Config Connector addon. The `proj-infra-admin` project and `kcc` cluster belong to the cloud infrastructure admin team who are responsible for configuring resources in GCP. You create a GCP Service Account with the required permissions and using [workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity) associate it with a Kubernetes service account used by the Config Connector to provision GCP resources. No other resources are provisioned in this project. The `proj-infra-admin` project and the `kcc` cluster are used solely for cloud resource deployment.

## Organization and Architecture

For the purpose of this guide, imagine your organization is divided into different teams based on functions. There are four teams in your organization:

- **Networking team** - a centralized team of cloud networking experts responsible for managing GCP networks. They own, manage and maintain the VPC and other networking resources for the entire organization.
- **Product bank team** - an application team responsible for creating the `bank` application or product. This team manages and maintains all resources to run the `bank` application. They maintain and manage their own Kubernetes clusters inside their own GCP project.
- **Product shop team** - an application team responsible for creating the `shop` application or product. This team manages and maintains all resources to run the `shop` application. They maintain and manage their own Kubernetes clusters inside their own GCP project.
- **Platform team** - in some cases, you may have a centralized platform administrator team. This team is responsible for maintaining and managing the platform where application teams run their applications. This team is responsible for GKE and service mesh administration.

In this guide, you are responsible for building a cloud architecture to run the two products - `bank` and `shop` in their own GCP projects.

## Objective

- Create three projects. One for the shared VPC (also the network host project) and two for the `bank` and `shop` product.
- Create and configure a shared VPC, create subnets and share subnets with the two service projects.
- Create two GKE private clusters in each service project (`bank` and `shop` project).
- Configure networking (NAT Gateways, Cloud Router and firewall rules) to allow inter-cluster and egress traffic from the four private GKE clusters. Also allow API service access from your terminal (Cloud Shell) to the four private GKE clusters using authorized networks.
- Deploy and configure multi project and multi cluster ASM to the four GKE clusters in multi-primary mode. Multi-primary mode deploys ASM controlplane in all clusters.
- Deploy the Bank of Anthos application on the two clusters in the `bank` project. All services except the databases are deployed as distributed services (Pods running on both clusters).
- Deploy the Online Boutique application on the two clusters in the `shop` project. All services except the Redis database are deployed as distributed services (Pods running on both clusters).
- Access the two applications via ASM Ingress.

## Costs

This tutorial uses the following billable components of Google Cloud Platform:

- [GKE](https://cloud.google.com/kubernetes-engine/pricing)
- [Networking](https://cloud.google.com/vpc/network-pricing#vpc-pricing)
- [Load Balancing](https://cloud.google.com/vpc/network-pricing#lb)

Use the [Pricing Calculator](https://cloud.google.com/products/calculator) to generate a cost estimate based on your projected usage.

## Before you begin

1.  In the Cloud Console, activate Cloud Shell.

    [ACTIVATE CLOUD SHELL](https://console.cloud.google.com/?cloudshell=true)

At the bottom of the [Cloud Console](https://cloud.google.com/shell/docs/features), a Cloud Shell session starts and displays a command-line prompt. Cloud Shell is a shell environment with the Cloud SDK already installed, including the [gcloud](https://cloud.google.com/sdk/gcloud) command-line tool, and with values already set for your current project. It can take a few seconds for the session to initialize.  
This guide requires a GCP organization because of shared VPC. Your user must also be an Organization Admin and a Billing Account Admin so you can create resources in the org and assign billing accounts to the new projects.  
This guide also requires a [gitlab.com](gitlab.com) account. You will create the infrastructure pipeline on gitlab.com.

## Setting up the environment

1.  Create a `WORKDIR` for this guide. At the end you can delete all the resources and this folder.

    ```bash
    mkdir -p asm-shared-vpc-multi-proj-gitlab && cd asm-shared-vpc-multi-proj-gitlab && export WORKDIR=`pwd`
    ```

1.  Create environment variables used in this guide. You use environment variables throughout this guide. It is best to save them in a `vars.sh` file, in case you lose access to your terminal (and your envars), you can source the vars.sh.

    ```bash
    export ADMIN_USER=[Enter Org admin email for example you@org.xyz]
    export ORG_NAME=[Your Organization name for example org.xyz]
    export BILLING_ACCOUNT=[Enter Billing Account ID for example C0FFEE-123456-AABBCC]
    export ORG_ID=[Enter Org ID for example 1234567890 from gcloud organizations list]
    ```

1.  Copy the remainder of the variables in your `vars.sh` file.

    ```bash
    export ORG_SHORT_NAME=${ORG_NAME%.*}
    export USER_NAME=${ADMIN_USER%@*}
    export USER_NAME=${USER_NAME:0:7}
    export SUBNET_1_REGION=us-west2
    export SUBNET_2_REGION=us-central1
    cat <<EOF >> $WORKDIR/vars.sh
    # Copy and paste the remainder of the variables
    export WORKDIR=$WORKDIR
    export ADMIN_USER=$ADMIN_USER
    export ORG_NAME=$ORG_NAME
    export BILLING_ACCOUNT=$BILLING_ACCOUNT
    export ORG_ID=$ORG_ID
    export ORG_SHORT_NAME=${ORG_SHORT_NAME}
    export USER_NAME=${USER_NAME}
    export KCC_FOLDER_NAME=${USER_NAME}-kcc
    export GITLAB_PROJECT_NAME=${USER_NAME}-asm-shared-vpc
    export GITLAB_BANK_PROJECT_NAME=${USER_NAME}-asm-bank-of-anthos
    export GITLAB_SHOP_PROJECT_NAME=${USER_NAME}-asm-online-boutique
    export GITLAB_CI_GCP_SA_NAME=gitlab-ci-sa
    export KCC_PROJECT=proj-infra-admin
    export KCC_GKE=kcc
    export KCC_GKE_ZONE=us-central1-f
    export KCC_SERVICE_ACCOUNT=kcc-sa
    export FOLDER_NAME=${USER_NAME}-asm-shared-vpc
    export HOST_NET_PROJECT=proj-0-net-prod
    export SVC_1_PROJECT=proj-1-bank-prod
    export SVC_2_PROJECT=proj-2-shop-prod
    export TERMINAL_IP=$(curl -s ifconfig.me)
    export GKE1=gke-1-r1a-prod
    export GKE2=gke-2-r1b-prod
    export GKE3=gke-3-r2a-prod
    export GKE4=gke-4-r2b-prod
    export GKE1_ZONE=${SUBNET_1_REGION}-a
    export GKE2_ZONE=${SUBNET_1_REGION}-b
    export GKE3_ZONE=${SUBNET_2_REGION}-a
    export GKE4_ZONE=${SUBNET_2_REGION}-b
    EOF
    ```

1.  Source vars.sh. In case you lose your terminal, you must always source vars.sh before proceeding.

    ```bash
    source $WORKDIR/vars.sh
    ```

1.  Install [krew](https://krew.sigs.k8s.io). Krew is a kubectl plugin manager that helps you discover and manage kubectl plugins. Two of these plugins are ctx and ns which make switching between multiple GKE clusters easier.

    ```bash
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )

    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc # .zshrc if using ZSH
    ```

1.  Install `ctx` and `ns` via krew for easy context switching.

    ```bash
    kubectl krew install ctx
    kubectl krew install ns
    kubectl krew install get-all
    ```

## Creating folder, project and GKE cluster for KCC

In this section, you create a folder and a project for KCC. This is a project owned by infrastructure administrators used to create GCP resources for example folders, projects, networks, IAM permissions and GKE clusters.

1.  Update `gcloud` and install alpha and beta components.

    ```bash
    gcloud components install alpha beta
    gcloud components update
    ```

In Cloud Shell, you may have to use `sudo apt-get install` to install gcloud components. Follow the instructions in Cloud Shell. This step may take a few minutes to complete.

1.  Login with your admin user account.

    ```bash
    gcloud config set account ${ADMIN_USER}
    gcloud auth login
    gcloud config unset project
    ```

1.  Create a folder for the KCC project. Creating a folder allows you to easily keep track of resources required for this guide in one place. At the end, you can delete all resources in this folder.

    ```bash
    gcloud alpha resource-manager folders create \
    --display-name=${KCC_FOLDER_NAME} \
    --organization=${ORG_ID}
    ```

1.  Get the KCC folder ID.

    ```bash
    export KCC_FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} | grep ${KCC_FOLDER_NAME} | awk '{print $3}')
    [ -z "$KCC_FOLDER_ID" ] && echo "KCC_FOLDER_ID is not exported" || echo "KCC Folder ID is $KCC_FOLDER_ID"
    echo -e "export KCC_FOLDER_ID=$KCC_FOLDER_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    KCC Folder ID is 1053315125805

1.  Create a project for KCC.

    ```bash
    gcloud projects create ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --folder ${KCC_FOLDER_ID}
    gcloud beta billing projects link ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --billing-account ${BILLING_ACCOUNT}

    gcloud services enable \
      --project ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      container.googleapis.com \
      cloudbilling.googleapis.com \
      cloudbuild.googleapis.com \
      cloudresourcemanager.googleapis.com

    gcloud container clusters create ${KCC_GKE} \
      --project=${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --zone=${KCC_GKE_ZONE} \
      --machine-type "e2-standard-4" \
      --num-nodes "4" --min-nodes "4" --max-nodes "6" \
      --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
      --release-channel regular \
      --addons ConfigConnector \
      --workload-pool=${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog

    touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    echo -e "export KUBECONFIG=$WORKDIR/asm-kubeconfig" >> $WORKDIR/vars.sh
    gcloud container clusters get-credentials ${KCC_GKE} --zone ${KCC_GKE_ZONE} --project ${KCC_PROJECT}-${KCC_FOLDER_ID}

    kubectl ctx ${KCC_GKE}=gke_${KCC_PROJECT}-${KCC_FOLDER_ID}_${KCC_GKE_ZONE}_${KCC_GKE}
    ```

## Creating an identity for KCC

In this section, you configure identity for KCC to create GCP resources. Config Connector creates and manages Google Cloud resources by authenticating with a Identity and Access Management (IAM) service account and using GKE's Workload Identity to bind IAM service accounts with Kubernetes service accounts.

1.  Create a service account and assign Organization Admin, Folder Admin, Project Creator, Billing Account Admin and Compute Admin roles to the account.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${KCC_SERVICE_ACCOUNT}

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.organizationAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/billing.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.folderAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.projectCreator'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'
    ```

1.  Give the KCC SA billing user permission to the billing account ID.

    ```bash
    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) += ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > kcc-sa-billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} kcc-sa-billing-iam-policy.json
    ```

1.  Create an IAM policy binding between the KCC GCP service account and the predefined Kubernetes service account that Config Connector runs.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts add-iam-policy-binding \
    ${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --member="serviceAccount:${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
    --role="roles/iam.workloadIdentityUser"
    ```

The output is similar to the following:

    Updated IAM policy for serviceAccount [kcc-sa@proj-infra-admin-389773461808.iam.gserviceaccount.com].
    bindings:
    - members:
      - serviceAccount:proj-infra-admin-389773461808.svc.id.goog[cnrm-system/cnrm-controller-manager]
      role: roles/iam.workloadIdentityUser
    etag: BwW6ev3b5kc=
    version: 1

1.  Create the config connector resource.

    ```bash
    cat <<EOF > ${KCC_GKE}-configconnector.yaml
    apiVersion: core.cnrm.cloud.google.com/v1beta1
    kind: ConfigConnector
    metadata:
      # the name is restricted to ensure that there is only one
      # ConfigConnector instance installed in your cluster
      name: configconnector.core.cnrm.cloud.google.com
    spec:
      mode: cluster
      googleServiceAccount: "${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com"
    EOF

    kubectl --context=${KCC_GKE} apply -f ${KCC_GKE}-configconnector.yaml
    ```

The output is similar to the following:

    configconnector.core.cnrm.cloud.google.com/configconnector.core.cnrm.cloud.google.com configured

## Creating an organization namespace

In this section, you create a namespace for organization. All organization level resources like folders or projects are created in the organization namespace.

1.  Create an org namespace.

    ```bash
    cat <<EOF > ${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
    apiVersion: v1
    kind: Namespace
    metadata:
      name: ${ORG_SHORT_NAME}
      annotations:
        cnrm.cloud.google.com/organization-id: "${ORG_ID}"
    EOF

    kubectl --context=${KCC_GKE} apply -f ${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
    ```

## Creating service account for Gitlab CI

In this section, you create a GCP Service Account in the `proj-infra-admin` project that is used by the Gitlab CI to deploy infrastructure.

1.  Create a GCP service for Gitlab CI. Assign the appropriate IAM roles that the Gitlab CI needs to deploy resources to GCP.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${GITLAB_CI_GCP_SA_NAME}

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/gkehub.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/container.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/owner'
    ```

1.  Download the Gitlab GCP SA credentials.

    ```bash
    gcloud iam service-accounts keys create ${GITLAB_CI_GCP_SA_NAME}-key.json \
    --iam-account ${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com
    ```

1.  Create a base64 version of the GCP SA credentials file.

    ```bash
    cat ${GITLAB_CI_GCP_SA_NAME}-key.json | base64 > ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt
    ```

## Creating the builder image for Gitlab CI

In this section, you create a Docker image with all the tools required to deploy resources for this guide.

1.  Clone the ASM repo.

    ```bash
    git clone https://gitlab.com/asm7/asm-shared-vpc.git asm-shared-vpc
    cd asm-shared-vpc/infrastructure/builder
    ```

1.  Create a `builder` Docker image which is used by Gitlab CI. This image contains all the tools required to deploy resources to GCP. These tools include gcloud, kubectl, and common bash utilities. Push the image to the GCR repo in the `proj-infra-admin` project.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds submit . --tag=gcr.io/${KCC_PROJECT}-${KCC_FOLDER_ID}/builder
    ```

The output is similar to the following:

    ...
    33e1791e-f1a4-4c50-98d0-ff63338d3a4b  2021-02-09T19:11:13+00:00  2M22S     gs://proj-infra-admin-1053315125805_cloudbuild/source/1612897872.028883-1ae85c6643044bc4b247da6dff4d4412.tgz  gcr.io/proj-infra-admin-1053315125805/builder (+1 more)  SUCCESS

1.  Make the `builder` Docker image public so that Gitlab CI can use the image.

    ```bash
    gsutil defacl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -r -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com

    cd ${WORKDIR}
    ```

## Creating Gitlab project

In this section, you create a Gitlab project and add the `kcc` cluster to the project.

1.  Create an access token to Gitlab so that you can use Gitlab APIs to create and configure projects. Access the following link. Name the token and check **api** under **Scopes**. Click **Create personal access token**.

    ```bash
    echo -n "https://gitlab.com/-/profile/personal_access_tokens"
    ```

<img src="/docs/gitlab-api-token.png" width=90% height=90%>

Copy the personal access token to a notepad or Google Keep app. It is important not to share this token with anyone since it provides complete read/write API access to your Gitlab account. You can revoke the token after you're done.

1.  Create a variable with your Gitlab personal access token.

    ```bash
    export GITLAB_TOKEN=<Gitlab personal access token>
    echo -e "export GITLAB_TOKEN=$GITLAB_TOKEN" >> $WORKDIR/vars.sh
    ```

1.  Create a new project using the Gitlab API.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${GITLAB_PROJECT_NAME}"
    ```

1.  Get your Gitlab user ID.

    ```bash
    export GITLAB_USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/user | jq -r '.id')
    [ -z "$GITLAB_USER_ID" ] && echo "GITLAB_USER_ID is not exported" || echo "GITLAB_USER_ID is $GITLAB_USER_ID"
    echo -e "export GITLAB_USER_ID=$GITLAB_USER_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_USER_ID is 12345679

1.  Get the Gitlab project ID of the project you just created.

    ```bash
    export GITLAB_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.id')
    [ -z "$GITLAB_PROJECT_ID" ] && echo "GITLAB_PROJECT_ID is not exported" || echo "GITLAB_PROJECT_ID is $GITLAB_PROJECT_ID"
    echo -e "export GITLAB_PROJECT_ID=$GITLAB_PROJECT_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_PROJECT_ID is 242937235

1.  Get Gitlab runner's registration code. You can use this code to deploy your Gitlab in the `kcc` cluster.

    ```bash
    export GITLAB_RUNNER_TOKEN=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID" | jq -r '.runners_token')
    [ -z "$GITLAB_RUNNER_TOKEN" ] && echo "GITLAB_RUNNER_TOKEN is not exported" || echo "GITLAB_RUNNER_TOKEN is $GITLAB_RUNNER_TOKEN"
    echo -e "export GITLAB_RUNNER_TOKEN=$GITLAB_RUNNER_TOKEN" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_RUNNER_TOKEN is abCDef1223DCba

Use helm v3 to deploy the runner in the `kcc` cluster. Otherwise, [install helm](https://helm.sh/docs/intro/install/).

1.  Verify you have helm v3 installed in your terminal.

    ```bash
    helm version
    ```

The output is similar to the following:

    version.BuildInfo{Version:"v3.5.0"...}

1.  Deploy the helm chart.

    ```bash
    helm repo add gitlab https://charts.gitlab.io

    kubectl --context=${KCC_GKE} create namespace gitlab

    helm install --namespace gitlab gitlab-runner --set gitlabUrl="https://gitlab.com" \
    --set runnerRegistrationToken="$GITLAB_RUNNER_TOKEN" \
    --set rbac.create=true \
    --set rbac.clusterWideAccess=true gitlab/gitlab-runner
    ```

The output is similar to the following:

    "NAME: gitlab-runner
    LAST DEPLOYED: Mon Feb  8 23:37:48 2021
    NAMESPACE: gitlab
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    Your GitLab Runner should now be registered against the GitLab instance reachable at: ""https://www.gitlab.com"""

1.  Wait until the Gitlab runner is Ready.

    ```bash
    kubectl --context=${KCC_GKE} -n gitlab wait --for=condition=available deployment gitlab-runner-gitlab-runner --timeout=5m
    ```

The output is similar to the following:

    deployment.apps/gitlab-runner-gitlab-runner condition met

1.  Disable shared runners on the project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X PUT "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID" --form "shared_runners_enabled=false"
    ```

1.  List the non-shared runners. You see the runner you just deployed.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

1.  Create variables in your Gitlab project which are used by the Gitlab CI pipeline.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=TERMINAL_IP" --form "value=${TERMINAL_IP}" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/"${GITLAB_PROJECT_ID}"/variables | jq
    ```

1.  Create an SSH keypair to use with Gitlab to clone and update repos.

    ```bash
    mkdir -p tmp && cd tmp
    ssh-keygen -t rsa -b 4096 \
    -C "${ADMIN_USER}" \
    -N '' \
    -f gitlab-key
    eval `ssh-agent`
    ssh-add ${WORKDIR}/tmp/gitlab-key
    echo -e "eval \`ssh-agent\` && ssh-add ${WORKDIR}/tmp/gitlab-key" >> $WORKDIR/vars.sh
    ```

1.  Add the SSH public key to your Gitlab account.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" --form "title=asm-shared-vpc-key" --form "key=$(cat gitlab-key.pub)"
    ```

1.  Get your Gitlab project's SSH URL.

    ```bash
    export GITLAB_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.ssh_url_to_repo')
    [ -z "$GITLAB_PROJECT_SSH_URL" ] && echo "GITLAB_PROJECT_SSH_URL is not exported" || echo "GITLAB_PROJECT_SSH_URL is $GITLAB_PROJECT_SSH_URL"
    echo -e "export GITLAB_PROJECT_SSH_URL=$GITLAB_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_PROJECT_SSH_URL is git@gitlab.com:ameer00/ameerab-asm-shared-vpc.git

1.  Clone the empty repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_PROJECT_SSH_URL ${ORG_SHORT_NAME}-asm-shared-vpc
    ```

The output is similar to the following:

    Cloning into 'org-asm-shared-vpc'...
    warning: You appear to have cloned an empty repository.

1.  Clone the ASM repo and copy the contents of the ASM repo into your empty repo.

    ```bash
    rm -rf asm-shared-vpc/.git
    cp -r asm-shared-vpc/. ${ORG_SHORT_NAME}-asm-shared-vpc
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${ORG_SHORT_NAME}-asm-shared-vpc
    git add .
    git commit -m "initial commit"
    git push
    ```

## Inspecting pipelines

In this section, you inspect the Gitlab pipelines which deploy resources in your GCP organization.

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    export GITLAB_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.web_url')
    echo -e "export GITLAB_PROJECT_WEB_URL=$GITLAB_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
    echo -n "${GITLAB_PROJECT_WEB_URL}"/-/pipelines
    ```

<img src="/docs/infra-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.  
This pipeline takes about 25 to 30 minutes to complete.

## Pipeline Stages

This section briefly explains what each stage in the pipeline accomplishes.

### Folder

This stage creates a folder where all of the projects are created. Creating a folder makes it easier to organize resources for this guide.

### Projects

This stage creates three projects. One project is the host network project owned by the network admin team where they create a shared VPC and all networking resources for the organization. The other two projects are service projects belonging to the application teams -- bank and shop. Each application team gets their own GCP project; however, both projects use the shared VPC for network resources.

### Network

This stage creates a VPC network in the host network project. It also creates two subnets and secondary ranges required for the GKE clusters. The two subnets created are in two different regions.

### SharedVPC

This stage configures the shared VPC. Host network project is configured as the host project and the other two projects (bank and shop) are added as service projects. Each service project is allowed one subnet that they can use for resources (for example GKE clusters) in that project.

### CloudNAT

This stage creates Cloud Nat gateways in the two regions. Cloud Nat gateways are used by private GKE clusters so that the Pods running inside GKE clusters can access the internet. This is also required by ASM, because the ASM controlplane Pods need access to all the GKE clusters' API servers. ASM controlplane Pods use Cloud Nat gateways in their region to access GKE API servers.

### GKE

This stage creates four GKE clusters. Two clusters each are created in the `bank` and `shop` projects. The clusters are configured as private clusters. This means that the nodes do not get a public IP address. The clusters are also configured with `master-authorized-networks` enabled. This means that only specific CIDR IP ranges are allowed access to the GKE API servers. GKE clusters are configured with both region Cloud Nat gateway public IP addresses as well as the Gitlab CI runner public IP.

### PrivateGKEFirewallRules

This stage creates the appropriate firewall rules required for Pods to communicate to each other across multiple GKE clusters.

### HubService

This stage creates a [Anthos GKE Hub Service Account](https://cloud.google.com/anthos/multicluster-management/connect/prerequisites#gke-cross-project) that is used to register GKE clusters to a different project. In this architecture, you register the four clusters to an [environ host project](https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project). You can register clusters from other projects to a single environ host project. In this architecture, you register all four GKE clusters (running in the `bank` and `shop` service projects) to the host network project. The host network project acts as the environ host project.

### ConfigureHub

This stage configures the Anthos GKE Hub service account IAM permissions so that the GKE clusters can register to the environ host project (or the host network project).

### RegisterGKE

This stage registers the four clusters to the Anthos GKE hub environ host project.

### ASMPrepProjects

This stage runs the [initialize script](https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm#setting_credentials_and_permissions) to prepare the projects for ASM installation. The script runs in the `bank` and the `shop` service projects.

### ASMGKEInstall

This stage installs [ASM](https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm#installing_anthos_service_mesh) on all four GKE clusters.

### ASMMulticlusterServiceDiscovery

This stage creates [kubeconfig secrets](https://cloud.google.com/service-mesh/docs/gke-install-multi-cluster#configure_endpoint_discovery_between_clusters) for all GKE clusters in all GKE clusters so that the ASM controlplane can automatically discover and update Services and Endpoints.

### ASMVerifyMulticluster

This stage deploys the [`whereami`](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/whereami) and [`sleep`](https://github.com/istio/istio/tree/master/samples/sleep) apps in a `sample` namespace in all four clusters. The `sleep` app is a simple curl utility and the `whereami` app responds with metadata regarding where the service and Pods are running. The stage uses the `sleep` app to curl the `whereami` Pods running in all four clusters from all four clusters to ensure ASM multicluster functionality.

## Accessing clusters

1. Get access to the GKE clusters.

   ```bash
   export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
   [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
   echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
   gcloud config set project ${SVC_1_PROJECT}-${FOLDER_ID}

   gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}

   kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE1_ZONE}_${GKE1}
   kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE2_ZONE}_${GKE2}
   kubectl ctx ${GKE3}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE3_ZONE}_${GKE3}
   kubectl ctx ${GKE4}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE4_ZONE}_${GKE4}
   ```

**Note** - If your terminal's public IP address changes, you will lose access to the GKE clusters. You will need to update the [`authorized networks`](https://cloud.google.com/kubernetes-engine/docs/how-to/authorized-networks) on all clusters with your terminal's new public IP. You can get your terminal's public IP from `curl ifconfig.me`. Or you can run the following script `$WORKDIR/asm-shared-vpc/scripts/update-clusters-auth-networks.sh`.

## Deploying Bank of Anthos

In this section, you deploy Bank of Anthos to GKE1 and GKE2 clusters in the `bank` service project using Gitlab project and Gitlab CI.

1.  Create a new Gitlab project for the Bank of Anthos application.

    ```bash
    cd $WORKDIR
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${GITLAB_BANK_PROJECT_NAME}"
    ```

1.  Get the Gitlab project ID of the project you just created.

    ```bash
    export GITLAB_BANK_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.id')
    [ -z "$GITLAB_BANK_PROJECT_ID" ] && echo "GITLAB_BANK_PROJECT_ID is not exported" || echo "GITLAB_BANK_PROJECT_ID is $GITLAB_BANK_PROJECT_ID"
    echo -e "export GITLAB_BANK_PROJECT_ID=$GITLAB_BANK_PROJECT_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_BANK_PROJECT_ID is 242937234

1.  Get Gitlab runner ID running in the `kcc` cluster. You can unlock this runner and use the same runner in multiple Gitlab projects.

    ```bash
    export GITLAB_RUNNER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)' | jq -r '.id')
    [ -z "$GITLAB_RUNNER_ID" ] && echo "GITLAB_RUNNER_ID is not exported" || echo "GITLAB_RUNNER_ID is $GITLAB_RUNNER_ID"
    echo -e "export GITLAB_RUNNER_ID=$GITLAB_RUNNER_ID" >> $WORKDIR/vars.sh
    ```

1.  Unlock the runner from the ASM project so that you can use the same runner in the `bank` and later in the `shop` projects.

    ```bash
    curl -s --request PUT --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/runners/${GITLAB_RUNNER_ID}" --form "locked=false"
    ```

1.  Enable the runner in the `bank` project.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/runners" --form "runner_id=${GITLAB_RUNNER_ID}"
    ```

1.  Disable shared runners on the project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X PUT "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID" --form "shared_runners_enabled=false"
    ```

1.  Verify that the runner is enabled in the `bank` project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

1.  Create variables in your Gitlab project which are used by the Gitlab CI pipeline.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/"${GITLAB_BANK_PROJECT_ID}"/variables | jq
    ```

1.  Get your Gitlab project's SSH URL.

    ```bash
    export GITLAB_BANK_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.ssh_url_to_repo')
    [ -z "$GITLAB_BANK_PROJECT_SSH_URL" ] && echo "GITLAB_BANK_PROJECT_SSH_URL is not exported" || echo "GITLAB_BANK_PROJECT_SSH_URL is $GITLAB_BANK_PROJECT_SSH_URL"
    echo -e "export GITLAB_BANK_PROJECT_SSH_URL=$GITLAB_BANK_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_BANK_PROJECT_SSH_URL is git@gitlab.com:ameer00/ameerab-asm-bank-of-anthos.git

1.  Clone the empty repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_BANK_PROJECT_SSH_URL ${ORG_SHORT_NAME}-asm-bank-of-anthos
    ```

The output is similar to the following:

    Cloning into 'org-asm-bank-of-anthos'...
    warning: You appear to have cloned an empty repository.

1.  Clone the ASM repo and copy the contents of the ASM repo into your empty repo.

    ```bash
    cp -r asm-shared-vpc/bank-of-anthos/. ${ORG_SHORT_NAME}-asm-bank-of-anthos
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${ORG_SHORT_NAME}-asm-bank-of-anthos
    git add .
    git commit -m "initial commit"
    git push
    ```

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    export GITLAB_BANK_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.web_url')
    echo -e "export GITLAB_BANK_PROJECT_WEB_URL=$GITLAB_BANK_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
    echo -n "${GITLAB_BANK_PROJECT_WEB_URL}"/-/pipelines
    ```

<img src="/docs/bank-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.  
This pipeline takes about 5 - 10 minutes to complete.  
This simple pipeline consists of three stages. The first stage deploys the application manifests to the clusters. The second stage verifies that the deployments are available and all Pods are running. The last stage outputs the `istio-ingressgateway` public IP address where the `frontend` Service is exposed. You can grab either IP addresses and access the Bank of Anthos application.

1.  Click on the last job called `get-frontend-ip-address` in the third stage (Access) and get the `istio-ingressgateway` public IP address from either of the two GKE clusters to access the Bank of Anthos application.

The output is similar to the following:

    GKE1 istio-ingressgateway public IP address is 35.235.83.250
    GKE2 istio-ingressgateway public IP address is 34.94.247.89

Access the application, login, create a deposit and transfer funds to other accounts. You can also create a new user and login using the new user and perform transactions.  
All features/services of the applications should be functional.

## Deploying Online Boutique

In this section, you deploy Online Boutique to GKE3 and GKE4 clusters in the `shop` project using Gitlab project and Gitlab CI.

1.  Create a new Gitlab project for the Online Boutique application.

    ```bash
    cd $WORKDIR
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${GITLAB_SHOP_PROJECT_NAME}"
    ```

1.  Get the Gitlab project ID of the project you just created.

    ```bash
    export GITLAB_SHOP_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.id')
    [ -z "$GITLAB_SHOP_PROJECT_ID" ] && echo "GITLAB_SHOP_PROJECT_ID is not exported" || echo "GITLAB_SHOP_PROJECT_ID is $GITLAB_SHOP_PROJECT_ID"
    echo -e "export GITLAB_SHOP_PROJECT_ID=$GITLAB_SHOP_PROJECT_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_SHOP_PROJECT_ID is 242937234

1.  Enable the runner in the `shop` project.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/runners" --form "runner_id=${GITLAB_RUNNER_ID}"
    ```

1.  Disable shared runners on the project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X PUT "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID" --form "shared_runners_enabled=false"
    ```

1.  Verify that the runner is enabled in the `bank` project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

1.  Create variables in your Gitlab project which are used by the Gitlab CI pipeline.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/"${GITLAB_SHOP_PROJECT_ID}"/variables | jq
    ```

1.  Get your Gitlab project's SSH URL.

    ```bash
    export GITLAB_SHOP_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.ssh_url_to_repo')
    [ -z "$GITLAB_SHOP_PROJECT_SSH_URL" ] && echo "GITLAB_SHOP_PROJECT_SSH_URL is not exported" || echo "GITLAB_SHOP_PROJECT_SSH_URL is $GITLAB_SHOP_PROJECT_SSH_URL"
    echo -e "export GITLAB_SHOP_PROJECT_SSH_URL=$GITLAB_SHOP_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_SHOP_PROJECT_SSH_URL is git@gitlab.com:ameer00/ameerab-asm-online-boutique.git

1.  Clone the empty repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_SHOP_PROJECT_SSH_URL ${ORG_SHORT_NAME}-asm-online-boutique
    ```

The output is similar to the following:

    Cloning into 'org-asm-online-boutique'...
    warning: You appear to have cloned an empty repository.

1.  Clone the ASM repo and copy the contents of the ASM repo into your empty repo.

    ```bash
    cp -r asm-shared-vpc/online-boutique/. ${ORG_SHORT_NAME}-asm-online-boutique
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${ORG_SHORT_NAME}-asm-online-boutique
    git add .
    git commit -m "initial commit"
    git push
    ```

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    export GITLAB_SHOP_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.web_url')
    echo -e "export GITLAB_SHOP_PROJECT_WEB_URL=$GITLAB_SHOP_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
    echo -n "${GITLAB_SHOP_PROJECT_WEB_URL}"/-/pipelines
    ```

    <img src="/docs/shop-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.  
This pipeline takes about 5 - 10 minutes to complete.  
This simple pipeline consists of three stages. The first stage deploys the application manifests to the clusters. The second stage verifies that the deployments are available and all Pods are running. The last stage outputs the `istio-ingressgateway` public IP address where the `frontend` Service is exposed. You can grab either IP addresses and access the Online Boutique application.

1.  Click on the last job called `get-frontend-ip-address` in the third stage (Access) and get the `istio-ingressgateway` public IP address from either of the two GKE clusters to access the Online Boutique application.

The output is similar to the following:

    GKE3 istio-ingressgateway public IP address is 34.121.7.75
    GKE4 istio-ingressgateway public IP address is 34.66.210.16

Access the application, browse through items, add them to your cart and checkout.  
All features/services of the applications should be functional.

## Cleaning up

To avoid incurring charges to your Google Cloud Platform account for the resources used in this tutorial:  
You can delete all the resources, the three projects and the folder you created for this guide. You can also delete the KCC folder and the `proj-infra-admin` project.  
When you create a shared VPC, a [lien](https://cloud.google.com/resource-manager/docs/project-liens) is placed on the network host project. The lien must be removed prior to deleting the network host project. The two service projects can be deleted.

1.  Delete the two service projects. You can undelete these projects within 30 days of deletion.

    ```bash
    source $WORKDIR/vars.sh
    export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
    [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
    gcloud projects delete "${SVC_1_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_2_PROJECT}-${FOLDER_ID}" --quiet
    ```

The output is similar to the following:

    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-1-bank-prod-814581352178].
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-2-shop-prod-814581352178].

1.  Remove the lien from the network host project and delete the host net project.

    ```bash
    export LIEN_ID=$(gcloud alpha resource-manager liens list --project="${HOST_NET_PROJECT}-${FOLDER_ID}" | awk 'NR==2 {print $1}')
    gcloud alpha resource-manager liens delete $LIEN_ID
    gcloud projects delete "${HOST_NET_PROJECT}-${FOLDER_ID}" --quiet
    ```

The output is similar to the following:

    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-0-net-prod-814581352178].

1.  Delete the folder

    ```bash
    gcloud resource-manager folders delete ${FOLDER_ID}
    ```

The output is similar to the following:

    Deleted [<Folder
     createTime: '2021-01-29T21:58:38.290Z'
     displayName: 'asm18-shared-vpc'
     lifecycleState: LifecycleStateValueValuesEnum(DELETE_REQUESTED, 2)
     name: 'folders/814581352178'
     parent: 'organizations/542306964273'>].

1.  Delete the `proj-infra-admin` project.

    ```bash
    gcloud projects delete "${KCC_PROJECT}-${KCC_FOLDER_ID}" --quiet
    ```

1.  Delete the KCC folder.

    ```bash
    gcloud resource-manager folders delete ${KCC_FOLDER_ID}
    ```

1.  Unset `KUBECONFIG`.

    ```bash
    unset KUBECONFIG
    ```

1.  Delete the three Gitlab projects.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID"
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID"
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID"
    ```

The output is similar to the following:

    {"message":"202 Accepted"}

1.  Delete the Gitlab SSH key.

    ```bash
    export GITLAB_SSH_KEY_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/keys | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.title=="asm-shared-vpc-key")' | jq -r '.id')

    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/user/keys/$GITLAB_SSH_KEY_ID"
    ```

1.  Remove KCC SA as a billing user from your billing account.

    ```bash
    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) -= ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > $WORKDIR/billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} $WORKDIR/billing-iam-policy.json
    ```

1.  Remove the KCC SA and Gitlab CI SA Organization IAM roles.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.organizationAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/billing.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.folderAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.projectCreator'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/gkehub.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/container.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/owner'
    ```

1.  Delete `WORKDIR`.

    ```bash
    cd $HOME
    rm -rf $WORKDIR
    ```

1.  Optionally, you can also revoke the API personal access token by navigating to the following page.

    ```bash
    echo -n "https://gitlab.com/-/profile/personal_access_tokens"
    ```

## [ASM Shared VPC with Multiple Service Projects: The Hard Way](/docs/manual.md)

Click on the link if you want to build the same architecture step by step using command line.

## Appendix

Documentation used for this guide.

- https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-shared-vpc
- https://cloud.google.com/anthos/multicluster-management/connect/prerequisites
- https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
- https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project
- https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm
- https://cloud.google.com/kubernetes-engine/docs/how-to/troubleshooting-and-ops#avmbr110_iam_permission_permission_missing_for_cluster_name

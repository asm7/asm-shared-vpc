#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo "ERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\n"
title_no_wait "*** BUILD INFRASTRUCTURE ***"
echo -e "\n"

echo -e "\n"
title_no_wait "Creating a WORKDIR folder..."
mkdir -p ${SCRIPT_DIR}/../../asm-shared-vpc-single-proj-gitlab
export WORKDIR=${SCRIPT_DIR}/../../asm-shared-vpc-single-proj-gitlab

if [ ! $ADMIN_USER ]; then echo "ADMIN_USER not defined"; exit 1; fi
if [ ! $ORG_NAME ]; then echo "ORG_NAME not defined"; exit 1; fi
if [ ! $BILLING_ACCOUNT ]; then echo "BILLING_ACCOUNT not defined"; exit 1; fi
if [ ! $ORG_ID ]; then echo "ADMIN_USER not defined"; exit 1; fi
if [ ! $GITLAB_TOKEN ]; then echo "GITLAB_TOKEN not defined"; exit 1; fi

echo -e "\n"
title_no_wait "Creating vars..."
export ORG_SHORT_NAME=${ORG_NAME%.*}
export USER_NAME=${ADMIN_USER%@*}
export USER_NAME=${USER_NAME:0:7}
export SUBNET_1_REGION=us-west2
export SUBNET_2_REGION=us-central1
cat <<EOF >> $WORKDIR/vars.sh
# Copy and paste the remainder of the variables
export WORKDIR=$WORKDIR
export ADMIN_USER=$ADMIN_USER
export ORG_NAME=$ORG_NAME
export BILLING_ACCOUNT=$BILLING_ACCOUNT
export ORG_ID=$ORG_ID
export GITLAB_TOKEN=$GITLAB_TOKEN
export ORG_SHORT_NAME=${ORG_SHORT_NAME}
export USER_NAME=${USER_NAME}
export KCC_FOLDER_NAME=${USER_NAME}-kcc-asm-shared-single
export GITLAB_PROJECT_NAME=${USER_NAME}-asm-shared-vpc-single-proj
export GITLAB_BANK_PROJECT_NAME=${USER_NAME}-asm-shared-vpc-single-proj-bank-of-anthos
export GITLAB_SHOP_PROJECT_NAME=${USER_NAME}-asm-shared-vpc-single-proj-online-boutique
export GITLAB_CI_GCP_SA_NAME=gitlab-ci-sa
export KCC_PROJECT=proj-infra-admin
export KCC_GKE=kcc
export KCC_GKE_ZONE=us-central1-f
export KCC_SERVICE_ACCOUNT=kcc-sa
export FOLDER_NAME=${USER_NAME}-asm-shared-single
export HOST_NET_PROJECT=proj-0-net-prod
export SVC_1_PROJECT=proj-1-ops-prod
export SVC_2_PROJECT=proj-2-bank-prod
export SVC_3_PROJECT=proj-3-shop-prod
export TERMINAL_IP=$(curl -s ifconfig.me)
export GKE1=gke-1-r1a-prod
export GKE2=gke-2-r1b-prod
export GKE3=gke-3-r2a-prod
export GKE4=gke-4-r2b-prod
export GKE1_ZONE=${SUBNET_1_REGION}-a
export GKE2_ZONE=${SUBNET_1_REGION}-b
export GKE3_ZONE=${SUBNET_2_REGION}-a
export GKE4_ZONE=${SUBNET_2_REGION}-b
export GKE5_INGRESS=ingress-config
export GKE5_INGRESS_ZONE=us-west2-a
EOF

source $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating KCC folder..."
gcloud config unset project

export KCC_FOLDER_EXISTS=$(gcloud alpha resource-manager folders list --organization=$ORG_ID | grep ${KCC_FOLDER_NAME})
if [ ! "$KCC_FOLDER_EXISTS" ]; then
    gcloud alpha resource-manager folders create \
    --display-name=${KCC_FOLDER_NAME} \
    --organization=${ORG_ID}
fi

echo -e "\n"
title_no_wait "Getting KCC folder ID..."
export KCC_FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} | grep ${KCC_FOLDER_NAME} | awk '{print $3}')
[ -z "$KCC_FOLDER_ID" ] && echo "KCC_FOLDER_ID is not exported" && exit 1 || echo "KCC Folder ID is $KCC_FOLDER_ID"
echo -e "export KCC_FOLDER_ID=$KCC_FOLDER_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating KCC GCP Project..."
if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
    echo "KCC project already exists."
else
    gcloud projects create ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --folder ${KCC_FOLDER_ID}
    gcloud beta billing projects link ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}
    gcloud services enable \
    --project ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    container.googleapis.com \
    cloudbilling.googleapis.com \
    cloudbuild.googleapis.com \
    sqladmin.googleapis.com \
    servicenetworking.googleapis.com \
    cloudresourcemanager.googleapis.com
fi

echo -e "\n"
title_no_wait "Creating KCC GKE cluster..."
export KCC_GKE_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} container clusters list)
if [ ! "$KCC_GKE_EXISTS" ]; then
    gcloud container clusters create ${KCC_GKE} \
    --project=${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --zone=${KCC_GKE_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "4" --min-nodes "4" --max-nodes "6" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --release-channel regular \
    --addons ConfigConnector \
    --workload-pool=${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog
fi

touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
echo -e "export KUBECONFIG=$WORKDIR/asm-kubeconfig" >> $WORKDIR/vars.sh
gcloud container clusters get-credentials ${KCC_GKE} --zone ${KCC_GKE_ZONE} --project ${KCC_PROJECT}-${KCC_FOLDER_ID}
kubectl ctx ${KCC_GKE}=gke_${KCC_PROJECT}-${KCC_FOLDER_ID}_${KCC_GKE_ZONE}_${KCC_GKE}

echo -e "\n"
title_no_wait "Creating KCC GCP SA and assigning IAM roles..."
export KCC_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${KCC_SERVICE_ACCOUNT})
if [ ! "$KCC_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${KCC_SERVICE_ACCOUNT}

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.organizationAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/billing.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.folderAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.projectCreator'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'

    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) += ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > $WORKDIR/kcc-sa-billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} $WORKDIR/kcc-sa-billing-iam-policy.json
fi

echo -e "\n"
title_no_wait "Creating WorkloadIdentity for the KCC GCP SA..."
gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts add-iam-policy-binding \
${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
--member="serviceAccount:${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
--role="roles/iam.workloadIdentityUser"

echo -e "\n"
title_no_wait "Creating ConfigConnector resource in the KCC cluster..."
cat <<EOF > $WORKDIR/${KCC_GKE}-configconnector.yaml
apiVersion: core.cnrm.cloud.google.com/v1beta1
kind: ConfigConnector
metadata:
  # the name is restricted to ensure that there is only one
  # ConfigConnector instance installed in your cluster
  name: configconnector.core.cnrm.cloud.google.com
spec:
  mode: cluster
  googleServiceAccount: "${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com"
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/${KCC_GKE}-configconnector.yaml

echo -e "\n"
title_no_wait "Creating Organization namespace resource in the KCC cluster..."
cat <<EOF > $WORKDIR/${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ${ORG_SHORT_NAME}
  annotations:
    cnrm.cloud.google.com/organization-id: "${ORG_ID}"
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml

echo -e "\n"
title_no_wait "Creating GCP service account for Gitlab CI runner and asigning IAM roles..."
export GITLAB_GCP_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${GITLAB_CI_GCP_SA_NAME})
if [ ! "$GITLAB_GCP_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${GITLAB_CI_GCP_SA_NAME}

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/gkehub.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/container.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/owner'
fi

echo -e "\n"
title_no_wait "Creating Gitlab GCP service account credentials..."
if [ ! -f "$WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json" ]; then
    gcloud iam service-accounts keys create $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json \
    --iam-account ${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com
    cat $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json | base64 > $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt
fi

echo -e "\n"
title_no_wait "Cloning the repo inside WORKDIR..."
if [ ! -d "$WORKDIR/asm-shared-vpc-single-project" ]; then
    git clone https://gitlab.com/asm7/asm-shared-vpc-single-project.git $WORKDIR/asm-shared-vpc-single-project
    cd $WORKDIR/asm-shared-vpc-single-project/infrastructure/builder
fi

echo -e "\n"
title_no_wait "Creating the builder docker image in GCR..."
export BUILDER_IMAGE_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds list | grep builder)
if [ ! "$BUILDER_IMAGE_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds submit $WORKDIR/asm-shared-vpc-single-project/infrastructure/builder/. --tag=gcr.io/${KCC_PROJECT}-${KCC_FOLDER_ID}/builder
    gsutil defacl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -r -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    cd ${WORKDIR}
fi

export GITLAB_USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/user | jq -r '.id')
[ -z "$GITLAB_USER_ID" ] && echo "GITLAB_USER_ID is not exported" || echo "GITLAB_USER_ID is $GITLAB_USER_ID"
echo -e "export GITLAB_USER_ID=$GITLAB_USER_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating the infrastructure Gitlab project..."
export GITLAB_PROJECT_ID_EXISTS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.id')
if [ ! "$GITLAB_PROJECT_ID_EXISTS" ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${GITLAB_PROJECT_NAME}"
fi

export GITLAB_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.id')
[ -z "$GITLAB_PROJECT_ID" ] && echo "GITLAB_PROJECT_ID is not exported" || echo "GITLAB_PROJECT_ID is $GITLAB_PROJECT_ID"
echo -e "export GITLAB_PROJECT_ID=$GITLAB_PROJECT_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Deploying Gitlab runner in the KCC cluster..."
export GITLAB_RUNNER_TOKEN=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID" | jq -r '.runners_token')
[ -z "$GITLAB_RUNNER_TOKEN" ] && echo "GITLAB_RUNNER_TOKEN is not exported" || echo "GITLAB_RUNNER_TOKEN is $GITLAB_RUNNER_TOKEN"
echo -e "export GITLAB_RUNNER_TOKEN=$GITLAB_RUNNER_TOKEN" >> $WORKDIR/vars.sh

helm repo add gitlab https://charts.gitlab.io

cat <<EOF > $WORKDIR/gitlab-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: gitlab
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/gitlab-namespace.yaml

export GITLAB_HELM_EXISTS=$(helm list -n gitlab -ojson | jq -r '.[].name')
if [ ! "$GITLAB_HELM_EXISTS" ]; then
    helm install --namespace gitlab gitlab-runner --set gitlabUrl="https://gitlab.com" \
    --set runnerRegistrationToken="$GITLAB_RUNNER_TOKEN" \
    --set rbac.create=true \
    --set rbac.clusterWideAccess=true gitlab/gitlab-runner
fi

kubectl --context=${KCC_GKE} -n gitlab wait --for=condition=available deployment gitlab-runner-gitlab-runner --timeout=5m

echo -e "\n"
title_no_wait "Disabling shared runners on the Gitlab project..."
curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X PUT "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID" --form "shared_runners_enabled=false"
curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'

echo -e "\n"
title_no_wait "Creating CI variables in the Gitlab project..."
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/variables" --form "key=TERMINAL_IP" --form "value=${TERMINAL_IP}" --form "protected=true"

curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/"${GITLAB_PROJECT_ID}"/variables | jq

echo -e "\n"
title_no_wait "Create a Gitlab SSH key..."
if [ ! -f ${WORKDIR}/tmp/asm-shared-single-gitlab-key ]; then
    mkdir -p $WORKDIR/tmp && cd $WORKDIR/tmp
    ssh-keygen -t rsa -b 4096 \
    -C "${ADMIN_USER}" \
    -N '' \
    -f $WORKDIR/tmp/asm-shared-single-gitlab-key
    eval `ssh-agent`
    ssh-add ${WORKDIR}/tmp/asm-shared-single-gitlab-key
    echo -e "eval \`ssh-agent\` && ssh-add ${WORKDIR}/tmp/asm-shared-single-gitlab-key" >> $WORKDIR/vars.sh
fi

curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" --form "title=asm-shared-vpc-key" --form "key=$(cat ${WORKDIR}/tmp/asm-shared-single-gitlab-key.pub)"

export GITLAB_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.ssh_url_to_repo')
[ -z "$GITLAB_PROJECT_SSH_URL" ] && echo "GITLAB_PROJECT_SSH_URL is not exported" || echo "GITLAB_PROJECT_SSH_URL is $GITLAB_PROJECT_SSH_URL"
echo -e "export GITLAB_PROJECT_SSH_URL=$GITLAB_PROJECT_SSH_URL" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Committing to the infrastructure pipeline..."
cd ${WORKDIR}
if [ ! -d ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project ]; then
    git clone $GITLAB_PROJECT_SSH_URL ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
fi

rm -rf ${WORKDIR}/asm-shared-vpc-single-project/.git
cp -r ${WORKDIR}/asm-shared-vpc-single-project/. ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project

cd ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
git add .
git commit -m "initial commit"
git push

echo -e "\n"
title_no_wait "Inspect the pipeline by clicking on the following link..."
export GITLAB_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.web_url')
echo -e "export GITLAB_PROJECT_WEB_URL=$GITLAB_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
echo -n "${GITLAB_PROJECT_WEB_URL}"/-/pipelines
echo -e "\n"

sleep 10

export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/pipelines | jq -r '.[].status')
while [ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]; do
    echo -e "Successful jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/jobs?scope[]=success" | jq '.[].name'
    echo -e "Running jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/jobs?scope[]=running" | jq '.[].name'
    echo -e "Pending jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/jobs?scope[]=pending" | jq '.[].name'
    echo -e "Upcoming jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/jobs?scope[]=created" | jq '.[].name'
    echo -e "Gitlab pipeline is $INFRA_GITLAB_PIPELINE_STATUS, sleeping for 10 seconds..."
    sleep 10
    export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/pipelines | jq -r '.[].status')
done

echo -e "\n"
title_no_wait "Getting cluster credentials to all clusters..."
export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
[ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
gcloud config set project ${SVC_1_PROJECT}-${FOLDER_ID}

gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
gcloud container clusters get-credentials ${GKE5_INGRESS} --zone ${GKE5_INGRESS_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}

kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE1_ZONE}_${GKE1}
kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE2_ZONE}_${GKE2}
kubectl ctx ${GKE3}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE3_ZONE}_${GKE3}
kubectl ctx ${GKE4}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE4_ZONE}_${GKE4}
kubectl ctx ${GKE5_INGRESS}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE5_INGRESS_ZONE}_${GKE5_INGRESS}


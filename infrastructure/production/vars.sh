#!/usr/bin/env bash
export KCC_GKE=kcc
export KCC_GKE_ZONE=us-central1-f

export HOST_NET_PROJECT=proj-0-net-prod
export SVC_1_PROJECT=proj-1-bank-prod
export SVC_2_PROJECT=proj-2-shop-prod

# Shared VPC
export SHARED_VPC=shared-vpc
export SUBNET_1_REGION=us-west2
export SUBNET_1_NAME=subnet-1-${SUBNET_1_REGION}
export SUBNET_1_RANGE=10.4.0.0/22
export SUBNET_1_POD_NAME=${SUBNET_1_NAME}-pods
export SUBNET_1_POD_RANGE=10.0.0.0/14
export SUBNET_1_SVC_1_NAME=${SUBNET_1_NAME}-svc-1
export SUBNET_1_SVC_1_RANGE=10.5.0.0/20
export SUBNET_1_SVC_2_NAME=${SUBNET_1_NAME}-svc-2
export SUBNET_1_SVC_2_RANGE=10.5.16.0/20
export SUBNET_1_SVC_3_NAME=${SUBNET_1_NAME}-svc-3
export SUBNET_1_SVC_3_RANGE=10.5.32.0/20
export SUBNET_2_REGION=us-central1
export SUBNET_2_NAME=subnet-2-${SUBNET_2_REGION}
export SUBNET_2_RANGE=10.12.0.0/22
export SUBNET_2_POD_NAME=${SUBNET_2_NAME}-pods
export SUBNET_2_POD_RANGE=10.8.0.0/14
export SUBNET_2_SVC_1_NAME=${SUBNET_2_NAME}-svc-1
export SUBNET_2_SVC_1_RANGE=10.13.0.0/20
export SUBNET_2_SVC_2_NAME=${SUBNET_2_NAME}-svc-2
export SUBNET_2_SVC_2_RANGE=10.13.16.0/20
export SUBNET_2_SVC_3_NAME=${SUBNET_2_NAME}-svc-3
export SUBNET_2_SVC_3_RANGE=10.13.32.0/20
export ASM_MAJOR_VERSION=1.8
export ASM_VERSION=1.8.3-asm.2
export ASM_LABEL=asm-183-2
export CLOUDSHELL_IP=$(curl ifconfig.me)

# GKE clusters
export GKE1=gke-1-r1a-prod
export GKE2=gke-2-r1b-prod
export GKE3=gke-3-r2a-prod
export GKE4=gke-4-r2b-prod
export GKE1_ZONE=${SUBNET_1_REGION}-a
export GKE2_ZONE=${SUBNET_1_REGION}-b
export GKE3_ZONE=${SUBNET_2_REGION}-a
export GKE4_ZONE=${SUBNET_2_REGION}-b
export GKE1_MASTER_IPV4_CIDR=172.16.0.0/28
export GKE2_MASTER_IPV4_CIDR=172.16.1.0/28
export GKE3_MASTER_IPV4_CIDR=172.16.2.0/28
export GKE4_MASTER_IPV4_CIDR=172.16.3.0/28
export GKE_INGRESS=ingress-config # for future use
export GKE_INGRESS_ZONE=us-west2-a # for future use